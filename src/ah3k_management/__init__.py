# -*- coding: utf-8 -*-
"""Management Software of the autonomous car"""

from ah3k_management import metadata

__version__ = metadata.version
__author__ = metadata.authors[0]
__license__ = metadata.license
__copyright__ = metadata.copyright