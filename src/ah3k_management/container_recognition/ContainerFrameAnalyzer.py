import copy

import cv2
import numpy as np

from ah3k_management.shared.ContainerColor import ContainerColor


class ContainerFrameAnalyzer:

    @property
    def color(self) -> ContainerColor:
        return self._color

    @color.setter
    def color(self, value: ContainerColor):
        self._color = value
        self.ycrcb_range = value.ycrcb_range

    def __init__(self, color: ContainerColor, show: bool):
        self.show = show
        self.color = color

    def has_container(self, frame):
        found = False
        marked = None


        width, height, channels = frame.shape

        roi = frame #[height/3:height, 0::]

        width, height, channels = roi.shape

        imgycrcb = cv2.cvtColor(roi, cv2.COLOR_RGB2YCrCb)


        arrMin, arrMax = self.ycrcb_range

        imgThresholded = cv2.inRange(imgycrcb, arrMin, arrMax)

        """erodeElmt = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 5))
        imgThresholded = cv2.erode(imgThresholded, erodeElmt)

        dilateElmt = cv2.getStructuringElement(cv2.MORPH_RECT, (10, 10))
        imgThresholded = cv2.dilate(imgThresholded, dilateElmt)"""

        sum_container = imgThresholded.sum() / 255

        #moments = cv2.moments(imgThresholded)
        #dM01 = moments['m01']
        #dM10 = moments['m10']
        #dArea = moments['m00']

        if sum_container > (width * height / 25):
            found = True
            #print("{} > {}".format(sum_container, width*height / 4))

            posX = int(width / 2) #  int(dM10 / dArea)
            posY = int(height / 2) #int(dM01 / dArea)

            self.iLastX = posX
            self.iLastY = posY

            if self.show:
                marked = copy.deepcopy(frame)
                cv2.circle(marked, (posY, posX), 16, (255, 0, 0), -1)
                cv2.circle(marked, (posY, posX), 32, (0, 255, 0), 0)

        if self.show:
            cv2.imshow("Thresholded Image", imgThresholded)

            # frame = frame + self.imgLine
            if marked is not None:
                cv2.imshow("Marked", marked)
            else:
                cv2.imshow("Marked", frame)


        return found
