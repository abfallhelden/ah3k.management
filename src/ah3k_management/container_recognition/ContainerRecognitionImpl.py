import logging
import threading

from ah3k_management.container_recognition.ContainerRecognizer import ContainerRecognizer
from ah3k_management.container_recognition.interface.ContainerRecognition import ContainerRecognition, \
    ContainerRecognitionDelegate
from ah3k_management.shared.ContainerColor import ContainerColor


class ContainerRecognitionImpl(ContainerRecognition):
    def __init__(self, show_video: bool):
        self.show_video = show_video

        try:
            self.recognizer = ContainerRecognizer(show_video)
        except OSError as e:
            logging.error("OSError({0}): {1}".format(e.errno, e.strerror))

    def start_tracking(self, color: ContainerColor):
        self.recognizer.set_color(color)
        self.recognizer_thread = threading.Thread(target=self.recognizer.run, name="ContainerRecognizer")
        self.recognizer_thread.start()
        logging.info("container recognizer started")

    def set_delegate(self, delegate: ContainerRecognitionDelegate):
        self.recognizer.set_delegate(delegate)

    def stop_tracking(self):
        self.recognizer.set_is_alive(False)
