import logging
import threading

import time

import cv2

from ah3k_management.container_recognition.SidewalkFrameAnalyzer import SidewalkFrameAnalyzer

try:
    from picamera import PiCamera
    from picamera.array import PiRGBArray
except OSError as e:
    logging.error("OSError({0}): {1}".format(e.errno, e.strerror))

from ah3k_management.container_recognition.ContainerFrameAnalyzer import ContainerFrameAnalyzer
from ah3k_management.container_recognition.interface.ContainerRecognition import ContainerRecognitionDelegate
from ah3k_management.shared.ContainerColor import ContainerColor
from ah3k_management.shared.FpsLogger import FpsLogger


class ContainerRecognizer:
    def __init__(self, show: bool = False, color: ContainerColor = ContainerColor.Green):
        self.color = color
        self.show = show
        self.is_alive = True

        self.camera = PiCamera()
        self.camera.resolution = (320, 240)
        self.camera.framerate = 20
        self.rawCapture = PiRGBArray(self.camera, size=(320, 240))

        self.fps_logger = FpsLogger("ContainerRecognition")
        self.frame_analyzer = ContainerFrameAnalyzer(self.color, self.show)
        self.sidewalk_analyzer = SidewalkFrameAnalyzer(self.show)

        self.iLastX = -1
        self.iLastY = -1

        # self.imgLine = np.zeros((720*factor, 1280*factor, 3), np.uint8)

    def set_is_alive(self, isAlive: bool):
        self.is_alive = isAlive
        if not self.is_alive:
            logging.info("If you kill me, well, you won't see containers!!")

    def run(self):

        threading.Timer(3, self._disable_auto_exposure).start()
        #self.camera.start_recording('/home/pi/container{}.h264'.format(time.strftime("%Y%m%d.%H%M%S")))

        for the_frame in self.camera.capture_continuous(self.rawCapture, format="bgr", use_video_port=True):
            frame = cv2.flip(the_frame.array, -1)

            found_container = self.frame_analyzer.has_container(frame)

            if found_container:
                self.delegate.container_detected()

            found_crossroad = self.sidewalk_analyzer.is_crossroad(frame)
            if found_crossroad:
                logging.info("detected a crossroad.")
                self.delegate.crossroad_detected()

            self.fps_logger.tick()
            self.rawCapture.truncate(0)

            if not self.is_alive:
                break

        #self.camera.stop_recording()
        logging.info("stopped container recognition")

    def _disable_auto_exposure(self):
        #logging.info("disabling auto exposure")
        #self.camera.exposure_mode = 'off'
        return

    def set_delegate(self, delegate: ContainerRecognitionDelegate):
        self.delegate = delegate

    def set_color(self, color: ContainerColor):
        self.color = color
        self.frame_analyzer.color = color
