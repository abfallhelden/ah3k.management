import copy
import logging

import cv2
from math import fabs

import numpy as np

ColorDifference_maxDif = 75
ColorDifference_minDif = 30

ColorDeviation_maxDeviation = 50

LineDirection_minRho = 11
LineDirection_maxRho = 80
LineDirection_maxTheta = 1.25
LineDirection_minTheta = 0.6

LineCheck_maxAmountOfLines = 40
LineCheck_minAmountOfLines = 0

Canny_THRESHOLD1 = 40
Canny_THRESHOLD2 = 50

Hough_Lines_THRESHOLD = 50
Hough_Lines_RHO = 2

dilate_element = np.array([[0, 0, 0, 1, 1],
                           [0, 0, 1, 1, 1],
                           [0, 1, 1, 1, 0],
                           [1, 1, 1, 0, 0],
                           [1, 1, 0, 0, 0]], np.uint8)
erode_element = np.array([[1, 0, 0, ],
                          [0, 1, 0],
                          [0, 0, 1]], np.uint8)


class SidewalkFrameAnalyzer:
    def __init__(self, show: bool):
        self.show = show

        self.checks = [self.create_regions,
                       self.previous_before_check,
                       self.color_difference_check,
                       self.color_deviation_check,
                       self.line_check,
                       self.line_direction_check,
                       self.previous_after_check]

        self.regions_defined = False
        self.history = [False, False, False]

    def is_crossroad(self, frame):
        self.frame = frame

        if not self.regions_defined:
            self.define_region_parameters()

        found = True
        logging.debug("before sidewalk checks")
        for check in self.checks:
            if not check():
                found = False
                break
        logging.debug("after sidewalk checks")

        if self.show:
            self.show_result(found)

        return found

    def show_result(self, found):
        marked_frame = copy.copy(self.frame)
        if found:
            border_color = (0, 255, 0)
        else:
            border_color = (0, 0, 255)
        cv2.rectangle(marked_frame, (self.region_front_x_start, self.region_front_y_start),
                      (self.region_front_x_end, self.region_front_y_end), self.color_front, -1)
        cv2.rectangle(marked_frame, (self.region_front_x_start, self.region_front_y_start),
                      (self.region_front_x_end, self.region_front_y_end), border_color, 2)
        cv2.rectangle(marked_frame, (self.region_back_x_start, self.region_back_y_start),
                      (self.region_back_x_end, self.region_back_y_end),
                      self.color_back, -1)
        cv2.rectangle(marked_frame, (self.region_back_x_start, self.region_back_y_start),
                      (self.region_back_x_end, self.region_back_y_end),
                      border_color, 2)
        cv2.imshow("sidewalk", marked_frame)

    def define_region_parameters(self):
        height, width, channels = self.frame.shape
        self.region_front_x_start = int(2)
        self.region_front_y_start = int(20)

        self.region_back_x_start = int(self.region_front_x_start + width / 3)
        self.region_back_y_start = int(self.region_front_y_start + height / 8)

        self.region_front_x_end = int(self.region_front_x_start + height / 32)
        self.region_front_y_end = int(self.region_front_y_start + height / 32)
        self.region_back_x_end = int(self.region_back_x_start + height / 32)
        self.region_back_y_end = int(self.region_back_y_start + height / 32)

        self.regions_defined = True

    def create_regions(self):
        self.region_front = self.frame[self.region_front_y_start:self.region_front_y_end,
                            self.region_front_x_start:self.region_front_x_end]
        self.region_back = self.frame[self.region_back_y_start:self.region_back_y_end,
                           self.region_back_x_start:self.region_back_x_end]
        return True

    def color_difference_check(self):
        self.color_front = cv2.mean(self.region_front)
        self.color_back = cv2.mean(self.region_back)
        avg_front = (self.color_front[0] + self.color_front[1] + self.color_front[2]) / 3
        avg_back = (self.color_back[0] + self.color_back[1] + self.color_back[2]) / 3
        dif = avg_front - avg_back
        found = ColorDifference_minDif < dif < ColorDifference_maxDif
        if self.show:
            print("dif: {}".format(int(dif)))

        logging.debug("dif: {}, found: {}".format(int(dif), found))
        return found

    def color_deviation_check(self):
        # deviation is the total difference between the r, g and b color values. small deviation means no color. (grey)
        deviation_front = fabs(self.color_front[0] - self.color_front[1]) + fabs(
            self.color_front[1] - self.color_front[2])
        deviation_back = fabs(self.color_back[0] - self.color_back[1]) + fabs(self.color_back[1] - self.color_back[2])
        if self.show:
            print("deviation_f: {}, deviation_b: {}".format(int(deviation_front), int(deviation_back)))
        found = deviation_front < ColorDeviation_maxDeviation and deviation_back < ColorDeviation_maxDeviation

        logging.debug("deviation_f: {}, deviation_b: {}, found: {}".format(int(deviation_front), int(deviation_back), found))
        return found

    def line_check(self):
        #self.frame = cv2.blur(self.frame, (4,4))
        region_edge = self.frame[self.region_front_y_start:self.region_back_y_end,
                      self.region_front_x_start:self.region_back_x_end]
        edges = cv2.Canny(region_edge, Canny_THRESHOLD1, Canny_THRESHOLD2)
        edges = cv2.dilate(edges, dilate_element)
        # edges = cv2.erode(edges, erode_element)

        self.lines = cv2.HoughLines(edges, Hough_Lines_RHO, np.pi / 180, Hough_Lines_THRESHOLD)

        self.region_edge_marked = copy.copy(region_edge)
        found = self.lines is not None and LineCheck_minAmountOfLines < len(self.lines) < LineCheck_maxAmountOfLines

        if self.show:
            cv2.imshow("edges", edges)
            if self.lines is not None:
                print("amount of lines: {}".format(len(self.lines)))

        logtext = ""
        if self.lines is not None:
            logtext = "amount of lines: {}, ".format(len(self.lines))
        logging.debug("{}found: {}".format(logtext, found))
        return found

    def line_direction_check(self):
        found = False
        for element in self.lines:
            rho, theta = element[0]
            if LineDirection_maxTheta > theta > LineDirection_minTheta and \
                                    LineDirection_maxRho > rho > LineDirection_minRho:
                found = True
                line_color = (0, 255, 0)

            else:
                line_color = (0, 0, 255)

            logging.debug("line (rho:{}, theta:{})".format(rho, theta))
            if self.show:
                print("line (rho:{}, theta:{})".format(rho, theta))
                a = np.cos(theta)
                b = np.sin(theta)
                x0 = a * rho
                y0 = b * rho
                x1 = int(x0 + 1000 * (-b))
                y1 = int(y0 + 1000 * (a))
                x2 = int(x0 - 1000 * (-b))
                y2 = int(y0 - 1000 * (a))

                cv2.line(self.region_edge_marked, (x1, y1), (x2, y2), line_color, 1)
                cv2.imshow("edges_line", self.region_edge_marked)

            if found:
                break
        logging.info("line direction found: {}".format(found))
        return found

    def previous_before_check(self):
        self.history[2] = self.history[1]
        self.history[1] = self.history[0]
        self.history[0] = False
        return True

    def previous_after_check(self):
        self.history[0] = True
        logging.debug("found history: {} {} {}".format(self.history[0],self.history[1],self.history[2]))

        if self.history[0] and self.history[1] and self.history[2]:
            logging.info("found 3 times")
            return True
        return False
