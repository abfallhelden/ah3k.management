from ah3k_management.shared.ContainerColor import ContainerColor


class ContainerRecognitionDelegate:
    def container_detected(self):
        pass

    def crossroad_detected(self):
        pass


class ContainerRecognition:
    def start_tracking(self, color: ContainerColor):
        """
        Start image processing for container recognition
        :param color: Color of the container that has to be recognized.
        """
        pass

    def stop_tracking(self):
        """
        Stop image processing
        """
        pass

    def set_delegate(self, delegate: ContainerRecognitionDelegate):
        pass
