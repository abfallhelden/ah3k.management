from ah3k_management.container_recognition.ContainerRecognitionImpl import ContainerRecognitionImpl
from ah3k_management.container_recognition.interface.ContainerRecognition import ContainerRecognition


class ContainerRecognitionFactory:
    """
    Factory for creating instance(s) of an implementation of a ContainerRecognition.
    """

    @staticmethod
    def get_container_recognition(show_video: bool) -> ContainerRecognition:
        """
        :return: Instance of ContainerRecognition.
        """
        return ContainerRecognitionImpl(show_video)
