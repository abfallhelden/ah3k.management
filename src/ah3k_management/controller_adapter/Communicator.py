import logging
from threading import Condition

from ah3k_management.controller_adapter.Message import Message

__author__ = 'michi'
import serial


class Communicator:
    def readline(self) -> str:
        pass

    def read(self) -> str:
        pass

    def send_line(self, line: str):
        pass

    def send_message(self, message: Message):
        self.send_line(message.to_string())


class FreedomBoardCommunicator(Communicator):
    encoding = 'UTF-8'

    def __init__(self, port):
        self.port = port
        pass

    def readline(self) -> str:
        eol = b'\n'
        leneol = len(eol)
        line = bytearray()
        while True:
            char = self.port.read(1)
            if char:
                line += char
                if line[-leneol:] == eol:
                    break
            else:
                break
        answer = line.decode("utf-8")
        if len(answer) > 0:
            logging.info("Controller -> Management: %s", answer[:-1])
        return answer

    def read(self) -> str:
        return self.port.read(1024).decode(self.encoding)

    def send_line(self, line: str):
        logging.info("Management -> Controller: %s", line)
        self.port.write(bytes(line + "\n", self.encoding))

    @staticmethod
    def freedom_board_port():
        return serial.Serial("/dev/ttyACM0", baudrate=38400, timeout=0.5)


class CommandLineCommunicator(Communicator):
    def __init__(self):
        self.answer = ""
        self.lock = Condition()
        self.usedefaultanswers = True

    def readline(self) -> str:
        """
        Read Answer from Controller to Management.
        :return:
        """
        self.lock.acquire()
        if len(self.answer) <= 0:
            self.lock.wait(2)
        self.lock.release()
        line = self.read()
        if len(line) > 0:
            logging.info("Controller -> Management: %s", line[:-1])
        return line

    def read(self) -> str:
        tmp = self.answer
        self.answer = ""
        return tmp

    def send_line(self, line: str):
        """
        Send line from Manager to Controller.
        :param line:
        :return:
        """
        logging.info("Management -> Controller: %s", line)
        if self.usedefaultanswers:
            if line == "getifversion":
                self.set_answer("ifversion 1.0.0\n")
            elif line == "init":
                self.set_answer("ready\n")
            elif line.startswith("speed"):
                self.set_answer("speedreached\n")

    def send_message(self, message: Message):
        self.send_line(message.to_string())

    def set_answer(self, answer: str):
        self.lock.acquire()
        self.answer = answer
        self.lock.notify_all()
        self.lock.release()
