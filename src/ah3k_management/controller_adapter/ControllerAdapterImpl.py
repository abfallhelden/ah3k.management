import logging
import threading

from ah3k_management.controller_adapter.Command import Command
from ah3k_management.controller_adapter.Communicator import Communicator
from ah3k_management.controller_adapter.ControllerListener import ControllerListener
from ah3k_management.controller_adapter.Message import Message
from ah3k_management.controller_adapter.interface.ControllerAdapter import ControllerAdapter, ControllerAdapterDelegate
from ah3k_management.shared.Status import Status


class ControllerAdapterImpl(ControllerAdapter):
    version = '1.0.0'

    def __init__(self, communicator: Communicator):
        self.communicator = communicator
        self.listener = ControllerListener(self.communicator)
        self.listenerthread = None

    def start_listener(self):
        if self.listenerthread:
            if self.listenerthread.is_alive():
                logging.warning("trying to start controller listener but was already running")
                return

        # init controller adapter
        logging.info('starting thread ControllerListener')
        self.listenerthread = threading.Thread(target=self.listener.run, name="ControllerListener")
        self.listenerthread.start()

    def stop_listener(self):
        logging.info('telling thread ControllerListener to stop')
        self.listener.end = True

    def set_delegate(self, delegate: ControllerAdapterDelegate):
        self.listener.set_delegate(delegate)

    def is_interface_compatible(self):
        controller_version = self.get_interface_version()
        result = controller_version == self.version
        if result:
            logging.info("interface version is compatible. controller:%s management:%s", controller_version,
                         self.version)
        else:
            logging.error("interface version mismatch! controller:%s management:%s", controller_version, self.version)
        return result

    def init(self):
        self.start_listener()

        # init controller
        self.send_line(Command.init)

    def set_status(self, status: Status):
        self.communicator.send_message(Message(Command.set_status).with_parameter(status.value))
        if status == Status.End:
            self.listener.end = 1

    def set_led(self, on: bool):
        if on:
            self.communicator.send_line("LED1 on")
        else:
            self.communicator.send_line("LED1 off")

    def speed(self, speed: int):
        self.communicator.send_message(Message(Command.speed).with_parameter(str(speed)))

    def stop_after(self, effective: int):
        self.communicator.send_message(Message(Command.stop_after).with_parameter(str(effective)))

    def stop_for_container_and_grab(self, distance: int):
        self.communicator.send_message(Message(Command.stop_for_container_and_grab).with_parameter(distance))

    def steer(self, radius: int):
        self.communicator.send_message(Message(Command.steer).with_parameter(str(radius)))

    def wait_for_clean_crossroad(self):
        self.communicator.send_message(Message(Command.wait_for_clean_crossroad))

    def drop_load(self):
        self.communicator.send_message(Message(Command.drop_load))

    def get_interface_version(self):
        self.communicator.send_message(Message(Command.get_interface_version))
        answer = self.wait_for_answer(Command.Answer.interface_version)
        return answer.parameters[0]

    def keep_alive(self):
        self.communicator.send_message(Message(Command.keep_alive))

    def send_line(self, line: str):
        self.communicator.send_line(line)

    def wait_for_answer(self, answer) -> Message:
        return self.listener.wait_for_answer(answer)
