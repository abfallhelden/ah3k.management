import logging
from threading import Condition

from ah3k_management.controller_adapter import Communicator
from ah3k_management.controller_adapter.Command import Command
from ah3k_management.controller_adapter.Message import Message
from ah3k_management.controller_adapter.interface.ControllerAdapter import ControllerAdapterDelegate
from ah3k_management.shared.ContainerColor import ContainerColor


class ControllerListener:
    def __init__(self, communicator: Communicator):
        self.communicator = communicator
        self.input_message_queue = []
        self.end = False
        self.is_running = False
        self.lock = Condition()

    def set_delegate(self, delegate: ControllerAdapterDelegate):
        self.delegate = delegate

    def run(self):
        logging.info("controller listener started")
        self.is_running = True
        while not self.end:
            self._read()
        self.is_running = False
        logging.info("controller listener thread ended")

    def wait_for_answer(self, answer: str) -> Message:
        self.lock.acquire()
        while not self._contains_message(answer):
            if self.is_running:  # for parallel reading
                logging.info("waiting now for answer %s", answer)
                self.lock.wait()
            else:  # for synchronized reading (in case thread was not started yet)
                self._read()

        logging.info("answer received (%s)", answer)
        message = self._handle_message_by_command(answer)
        self.lock.release()

        return message

    def _read(self):
        communication_input = str(self.communicator.readline())
        lines = communication_input.splitlines()
        if len(lines) > 0:
            self.lock.acquire()
            for line in lines:
                logging.debug("received line %s", line)
                message = Message(str(line))
                if not self._handle_message_for_delegate(message):
                    logging.warning("skipping message ({})".format(message))
            self.lock.notify()
            self.lock.release()

    def _contains_message(self, answer: str) -> bool:
        for message in self.input_message_queue:
            if message.command == answer:
                return True
        return False

    def _handle_message_by_command(self, command: str) -> Message:
        for message in self.input_message_queue:
            if message.command == command:
                self.input_message_queue.remove(message)
                return message

    def _handle_message_for_delegate(self, message: Message) -> bool:
        logging.debug('handling message for delegate (%s)', message.to_string())
        if message.command == Command.Answer.speed_reached:
            self.delegate.speed_reached()

        elif message.command == Command.Answer.container_dropped:
            self.delegate.container_dropped()

        elif message.command == Command.Answer.container_not_found:
            self.delegate.container_not_found()

        elif message.command == Command.Answer.crossroad_clean:
            self.delegate.crossroad_clean()

        elif message.command == Command.Answer.load_dropped:
            self.delegate.load_dropped()

        elif message.command == Command.Answer.start:
            if message.parameters[0] == "0":
                self.delegate.start(ContainerColor.Green)
            elif message.parameters[0] == "1":
                self.delegate.start(ContainerColor.Blue)

        elif message.command == Command.Answer.ready:
            self.delegate.ready()

        elif message.command == Command.Answer.battery_low:
            self.delegate.battery_low()

        elif message.command == Command.Answer.interface_version:
            self.input_message_queue.append(message)

        elif message.command == Command.Answer.error:
            logging.error('Controller Error: %s', message.to_string())

        else:
            logging.warning('no handler was found for message %s', message.to_string())
            return False
        return True
