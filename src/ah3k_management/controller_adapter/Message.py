class Message(object):
    def __init__(self, line: str):
        elements = line.split(" ")
        self.command = elements[0]
        self.parameters = elements[1::]

    def with_parameter(self, parameter):
        """
        Set parameter  [for fluent code].
        :param parameter: Parameter to set.
        :return: self
        """
        self.parameters = [str(parameter)]
        return self

    def with_parameters(self, parameters: [str]):
        """
        Set parameters  [for fluent code].
        :param parameters: Parameters to set.
        :return: self
        """
        self.parameters = parameters
        return self

    def to_string(self):
        """
        Converts the Message to a string ready to be sent to the controller.
        :return: string.
        """
        if len(self.parameters) > 0:
            return "{} {}".format(self.command, " ".join(self.parameters))
        else:
            return self.command
