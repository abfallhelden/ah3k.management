from ah3k_management.shared import Status
from ah3k_management.shared.ContainerColor import ContainerColor


class ControllerAdapterDelegate(object):
    """
    Delegate for receiving communication from Controller.
    """

    def ready(self):
        """
        The initialization process ended properly and the controller is ready.
        """
        pass

    def start(self, container_color: ContainerColor):
        """
        Notification that the start button was pressed by the user.
        :param container_color: The color of the container to be grabbed.
        """
        pass

    def speed_reached(self):
        """
        The requested speed was reached.
        """
        pass

    def crossroad_clean(self):
        """
        Notification when there is no object at the crossroad.
        """
        pass

    def battery_low(self):
        """
        Notification whene the battery is running out and we have to shut down.
        """
        pass

    def container_dropped(self):
        """
        Notification, that the process of emptying container was finished.
        """
        pass

    def load_dropped(self):
        """
        Notification, that the process of emptying the load was finished.
        """
        pass

    def container_not_found(self):
        """
        Notification, that the container was not recognized with distance sensor.
        """
        pass


# Do not implement Methods here.


class ControllerAdapter(object):
    """
    Interface for the controller Adapter.
    """

    version = "0"
    """ The Version of the Interface implemented by this interface class."""

    def set_delegate(self, delegate: ControllerAdapterDelegate):
        pass

    def is_interface_compatible(self) -> bool:
        """
        Checks if the interface version installed on the controller is compatible with the
        interface version installed on ah3k.management.
        :return: true if compatible, false if not compatible.
        """
        pass

    def init(self):
        """
        Initialize the controller board.
        """
        pass

    def set_status(self, status: Status):
        """
        Update the status of state machine on the microcontroller and change the status display.
        :param status: the new status.
        """
        pass

    def set_led(self, on: bool):
        pass

    def speed(self, speed: int):
        """
        Sets the speed of the vehicle.
        speed > 0  -> drive forward
        speed = 0  -> stand still
        speed < 0  -> drive backward
        :param speed: The speed to be reached [mm/s]
        """
        pass

    def stop_after(self, effective: int):
        """
        Stops the vehicle after a specific distance.
        :param effective: After how many [mm] the speed has to be reached
        :return:
        """

    def stop_for_container_and_grab(self, distance: int):
        """
        Tells the Controller to stop when it tracks the container.
        :param distance: Calculated Distance to container.
        """
        pass

    def steer(self, radius: int):
        """
        Steer the vehicle in a specific radius.
        radius > 0  -> to the right
        radius = 0  -> straight
        radius < 0  -> to the left
        :param radius:
        """
        pass

    def wait_for_clean_crossroad(self):
        """
        Lets the controller wait until there is no object at the crossroad.
        This command is asynchronous.
        """
        pass

    def drop_load(self):
        """
        Drops the load in target area.
        """
        pass

    def get_interface_version(self) -> str:
        """
        Get Interface Version on Controller.
        :return: version
        """
        return self.version

    def keep_alive(self):
        """
        Sends a keep alive message.
        """
        pass

    def start_listener(self):
        """
        The Controller Adapter will start listening to Messages from the Controller.
        """
        pass

    def stop_listener(self):
        """
        The Controller Adapter will stop listening to Messages from the Controller.
        """
        pass
