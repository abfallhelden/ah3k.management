import logging
import threading

import cv2

from ah3k_management.line_detection.LineTracker import LineTracker
from ah3k_management.line_detection.RegionContour import RegionContour
from ah3k_management.line_detection.RegionEndline import RegionEndline
from ah3k_management.line_detection.interface.LineDetection import LineDetection
from ah3k_management.line_detection.interface.LineDetection import LineDetectionDelegate


class LineDetectionImpl(LineDetection):

    def __init__(self, cam_index, treshold: int, show_video: bool):
        """
        Create instance of LineTracker
        Define the rois and set initial status
        :return:
        """
        logging.info("Initializing line tracker with video %s", cam_index)
        self.cam_index = cam_index
        self.show_video = show_video

        self.size_factor = 0.5 #self.width / self.referenceWidth

        self.height = 720 * self.size_factor  #360
        self.width = 1280 * self.size_factor  #640
        self.roi_width_factor = 0.1
        self.roiWidth = self.width * self.roi_width_factor

        self.tresh = float(treshold)
        logging.info("Treshold set to %s", self.tresh)

        self.tracker = LineTracker(self.size_factor, self.cam_index, self.show_video)
        #self.tracker = LineTracker(self.size_factor, self.cam_index, True)
        self.tracker.set_rois(self.define_rois())
        self.tracker.set_crossroad_endline_roi(self.define_rois_c())

        self.trackLineRun = True
        self.statusChanged = False
        logging.info("LineDetectionImpl instance created")

    def set_delegate(self, delegate:LineDetectionDelegate):
        """
        Operator set the delegate for receiving notifications
        :param delegate: instance of delegate
        :return:
        """
        self.lineDetectionDelegate = delegate
        self.tracker.set_delegate(delegate)
        logging.info("LineDetectionDelegate set")

    def start_tracking(self):
        """
        Start Line Detection ==> Start Tracker Thread
        :return:
        """
        self.trackerThread = threading.Thread(target=self.tracker.run, name="LineTracker")
        self.trackerThread.start()
        self.set_tresh_tresh(84)
        threading.Timer(2, self.set_tresh_tresh_for_normal_road, ()).start()
        logging.info("tracker started")

    def stop_tracking(self):
        """
        Stop Line Detection ==> Stop Tracker Thread
        :return:
        """
        self.tracker.set_is_alive(False)
        logging.info("tracker stopped")

    def look_for_endline(self):
        """
        Start Endline Detection
        :return:
        """
        self.tracker.check_for_endline()
        self.set_tresh_tresh(86)
        logging.info("Check for Endline")

    def define_rois_c(self):

        roi_c = RegionEndline(1, int(6*self.height / 10), int(self.height), int(self.roiWidth * 5), int(self.roiWidth * 10), self.tresh, False)
        return roi_c

    def define_rois(self):
        """
        Define all instances of Regions
        :return: list of regions
        """
        self.roiList = [RegionContour(10, 0, int(self.height), int(self.roiWidth*9), int(self.roiWidth*10), self.tresh, 10, self.height),
                        RegionContour(9, 0, int(self.height), int(self.roiWidth*8), int(self.roiWidth*9), self.tresh, 10, self.height),
                        RegionContour(8, 0, int(self.height), int(self.roiWidth*7), int(self.roiWidth*8), self.tresh, 10, self.height)]

        logging.info("rois defined")

        return self.roiList


    def set_tresh_tresh_for_normal_road(self):
        self.set_tresh_tresh(78)

        logging.debug("setting tresh_tresh to 78")
    def set_tresh_tresh(self, tresh_tresh):
        for roi in self.roiList:
            roi.tresh_tresh = tresh_tresh
