import logging

import cv2

from ah3k_management.line_detection import RegionEndline
from ah3k_management.line_detection.RegionContour import RegionContour
from ah3k_management.line_detection.interface.LineDetection import LineDetectionDelegate
from ah3k_management.line_detection.interface.LineInfo import LineInfo
from ah3k_management.shared.FpsLogger import FpsLogger


class LineTracker(object):
    def __init__(self, factor: float, cam_index, show: bool = False):
        """
        Cap the cam
        Set Camera properties
        :return:
        """

        self.cam_index = cam_index
        self.cap = cv2.VideoCapture(self.cam_index)

        self.cap.set(3, 1280 * factor)  # width)
        self.cap.set(4, 720 * factor)  # height)
        #self.cap.set(cv2.CAP_PROP_AUTO_EXPOSURE, 0)

        self.show = show
        self.isAlive = True
        self.factor = factor

        self.lookForEndline = False

        self.fps_logger = FpsLogger("LineDetection")

    def set_rois(self, roiList: [RegionContour]):
        """
        Set list of regions
        """
        self.roiList = roiList

    def set_delegate(self, delegate: LineDetectionDelegate):
        """
        set delegate
        :param delegate:
        :return:
        """
        self.lineDetectionDelegate = delegate

    def set_crossroad_endline_roi(self, roi_c: RegionEndline):
        self.roi_c = roi_c

    def create_control_roi_above(self):
        pass

    def create_control_roi_below(self):
        pass

    def check_for_endline(self):
        self.lookForEndline = True

    def set_is_alive(self, isAlive: bool):
        self.isAlive = isAlive
        if self.isAlive == False:
            logging.info("NO I don't wanna die :(")

    def run(self):
        """
        Detect the line
        :return:
        """
        points_1 = []
        #imgcounter=0
        #self.imgC = 0
        while self.isAlive:
            points_1.clear()
            ret, frame = self.cap.read()
            """
            ret = True
            img = cv2.imread("E_70_Zieleinfahrt2/img" + str(self.imgC) + ".jpg")
            frame = img
            """
            distance = -90000

            if ret == False:
                logging.error("Cannot get frame")
            else:
                point = -1
                for roi in self.roiList:
                    roi.set_frame(frame)
                    roi.convert_rgb_gray()
                    roi.threshold_bitwise()
                    #roi.erode_dilate()
                    roi.find_contours()
                    roi.search_contours()
                    point = roi.return_line_info()
                    if self.show:
                        cv2.rectangle(frame, (roi.get_x1(),roi.get_y1()), (roi.get_x2(),roi.get_y2()), (255,0,0), 2)
                    if point != -1:
                        points_1.append(point)
                        break


                #CHECK FOR ENLINE

                if self.lookForEndline:
                    self.roi_c.set_frame(frame)
                    self.roi_c.convert_rgb_gray()
                    self.roi_c.threshold_bitwise()
                    self.roi_c.find_contours()
                    self.roi_c.search_contours()
                    if self.show:
                        cv2.rectangle(frame, (self.roi_c.get_x1(),self.roi_c.get_y1()), (self.roi_c.get_x2(),self.roi_c.get_y2()), (255,0,0), 2)
                    if self.roi_c.return_line_info():
                        self.lineDetectionDelegate.end_line_detected()
                        print("Endline")
                #CHECK FOR ENDLINE FINISHED

                #cv2.imwrite("/home/pi/pics/img" + str(imgcounter) + ".jpg" , frame)
                if self.show:
                    cv2.imshow("frame", frame)

                for point in points_1:
                    if point != -1:
                        distance = point
                        break

                if distance > -90000:
                    lineInfo = LineInfo(distance)
                    self.lineDetectionDelegate.line_info(lineInfo)
                self.fps_logger.tick()

                #imgcounter += 1

            #self.imgC += 1
            """
            if cv2.waitKey(0) & 0xFF == ord('q'):
               break
            """

        logging.info("Releasing Camera")
        cv2.destroyAllWindows()
        self.cap.release()

