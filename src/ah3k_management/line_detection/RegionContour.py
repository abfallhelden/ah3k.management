from ah3k_management.line_detection.RegionOfInterest import RegionOfInterest
from ah3k_management.line_detection.interface.Region import Region
import cv2
import logging


class RegionContour(RegionOfInterest):
    def __init__(self, roi_id: int, y1: int, y2: int, x1: int, x2: int, tresh: int, mmToCar: int, height: float,
                 debug: bool = False):
        """
        Set roi Attributes
        :param roi_id: unique ID to identify the roi(for debug purposes)
        :param y1: y-coordinate of top-left corner(in px)
        :param y2: y-coordinate of bottom-left corner(in px)
        :param x1: x-coordinate of top-left corner(in px)
        :param x2: x-coordinate of top-right corner(in px)
        :param tresh: [PLACEHOLDER]default treshold property[PLACEHOLDER] ---> Replace with tresh from config
        :param mmToCar: Distance from image bottom to the car
        :return:
        """
        super().__init__()
        self.roi_id = roi_id
        self.y1 = y1
        self.y2 = y2
        self.x1 = x1
        self.x2 = x2
        self.tresh = tresh
        self.mmToCar = mmToCar
        self.height = height
        self.debug = debug
        self.erodeElmt = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 5))
        self.dilateElmt = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 5))
        self.mmProPx = 0.09861111111111111 * 720 / height
        logging.debug("Region " + str(roi_id) + " created")

    def search_contours(self):
        """
        Filter the detected Contours
        :return:
        """
        self.lowestPoint = -1
        for i in self.contours:
            area = cv2.contourArea(i)
            moments = cv2.moments(i)
                                    #evtl. höher, z.B.3500
            if area > 2000 and area < 3200 and moments['m00'] != 0.0 and moments['m01'] != 0.0:  # WENN 10 ROIs MAIN
            #if area > 2000 and moments['m00'] != 0.0 and moments['m01'] != 0.0:  # WENN 10 ROIs MAIN
                cy = int(moments['m01'] / moments['m00'])

                if self.lowestPoint == -1 or self.lowestPoint < cy:
                    self.lowestPoint = cy
                    self.lowestContour = i

    def return_line_info(self):
        mmToBottom = -1
        if self.lowestPoint != -1:
            """
            approx = cv2.approxPolyDP(self.lowestContour,0.01*cv2.arcLength(self.lowestContour,True),True)
            if len(approx) == 4:
            """
            moments = cv2.moments(self.lowestContour)
            cx = int(moments['m10'] / moments['m00'])
            cy = int(moments['m01'] / moments['m00'])
            x, y, w, h = cv2.boundingRect(self.lowestContour)
            diff = h / w
            if diff < 1.2:  # 1.0
                pxToBottom = int(self.height - cy - (h / 2))
                mmToBottom = int(pxToBottom * self.mmProPx)
                if self.debug:
                    cv2.circle(self.frame, (cx + self.x1, cy + self.y1), 4, (255, 0, 0), -1)
                    cv2.circle(self.frame, (cx + self.x1, cy + self.y1), 8, (0, 255, 0), 0)
                    x, y, w, h = cv2.boundingRect(self.lowestContour)
                    cv2.rectangle(self.frame, (x + self.x1, y + self.y1), (x + w + self.x1, y + h + self.y1),
                                  (0, 255, 0),
                                  2)
        return mmToBottom
