import numpy as np

from ah3k_management.line_detection.RegionOfInterest import RegionOfInterest
from ah3k_management.line_detection.interface.Region import Region
import cv2
import logging


class RegionEndline(RegionOfInterest):

    def __init__(self, roi_id: int, y1: int, y2: int, x1: int, x2: int, tresh: int, debug: bool = False):
        """
        Set roi Attributes
        :param roi_id: unique ID to identify the roi(for debug purposes)
        :param y1: y-coordinate of top-left corner(in px)
        :param y2: y-coordinate of bottom-left corner(in px)
        :param x1: x-coordinate of top-left corner(in px)
        :param x2: x-coordinate of top-right corner(in px)
        :param tresh: [PLACEHOLDER]default treshold property[PLACEHOLDER] ---> Replace with tresh from config
        :param mmToCar: Distance from image bottom to the car
        :return:
        """
        self.roi_id = roi_id
        self.y1 = y1
        self.y2 = y2
        self.x1 = x1
        self.x2 = x2
        self.tresh = 130 #tresh
        self.debug = debug
        self.line_counter = 0
        self.line_detected_frames = 0
        self.erodeElmt = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 5))
        self.dilateElmt = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 5))
        self.framecounter = 0

        logging.debug("Region Endline" + str(roi_id) + " created")

    def search_contours(self):
        self.lowestPoint = -1
        for i in self.contours:
            # Moments auslesen
            area = cv2.contourArea(i)
            moments = cv2.moments(i)
                    #vorher 2500
            if area > 1500 and moments['m00'] != 0.0 and moments['m01'] != 0.0:
                cy = int(moments['m01'] / moments['m00'])  # cy = M01/M00

                if self.lowestPoint == -1 or self.lowestPoint < cy:
                    self.lowestPoint = cy
                    self.lowestContour = i

    def return_line_info(self):
        if self.lowestPoint != -1:
            # Moments auslesen
            moments = cv2.moments(self.lowestContour)

            #self.line_counter += 1
            self.line_detected_frames += 1

            if self.debug:
                cx = int(moments['m10'] / moments['m00'])
                cy = int(moments['m01'] / moments['m00'])
                cv2.circle(self.frame, (cx + self.x1, cy + self.y1), 4, (255, 0, 0), -1)
                cv2.circle(self.frame, (cx + self.x1, cy + self.y1), 8, (0, 255, 0), 0)
                x, y, w, h = cv2.boundingRect(self.lowestContour)
                cv2.rectangle(self.frame, (x + self.x1, y + self.y1), (x + w + self.x1, y + h + self.y1),
                              (0, 255, 0),
                              2)

        else:
                                        #vorher 3
            if self.line_detected_frames >= 3:
                #print("Linecounter increment")
                self.line_counter += 1
                self.line_detected_frames = 0

        if self.line_counter == 1:
            self.framecounter += 1

        if self.framecounter > 100:
            self.framecounter = 0
            self.line_counter = 0

        if self.line_counter >= 3:
            #print("Endline!!!")
            return True

        return False

    def threshold_bitwise(self):
        """
        GAUSSIAN BLUR?
        Treshold the image
        :return:
        """
        ret, self.roiImg = cv2.threshold(self.roiImg, self.tresh, 255, 0)
