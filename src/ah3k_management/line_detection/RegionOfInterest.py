import cv2
import numpy as np

from ah3k_management.line_detection.interface.Region import Region


class RegionOfInterest(Region):

    def __init__(self):
        self.tresh_tresh = 81

    def get_id(self):
        return self.roi_id

    def get_x1(self):
        return self.x1

    def get_x2(self):
        return self.x2

    def get_y1(self):
        return self.y1

    def get_y2(self):
        return self.y2

    def set_frame(self, frame: object):
        """
        Set image to process
        Call method create_roi()
        :param frame: Image read from camera
        :return:
        """
        self.frame = frame
        self._create_roi()

    def _create_roi(self):
        """
        Place the roi on the frame with the x- and y properties
        :return:
        """
        self.roi = self.frame[self.y1:self.y2, self.x1:self.x2]

    def convert_rgb_gray(self):
        """
        Convert image to grayscale
        :return:
        """
        self.roiImg = cv2.cvtColor(self.roi, cv2.COLOR_BGR2GRAY)

    def threshold_bitwise(self):
        """
        GAUSSIAN BLUR?
        Treshold the image
        :return:
        """
        hist = cv2.calcHist([self.roiImg],[0],None,[256],[0,256])
        dyn_tresh = np.percentile(hist, self.tresh_tresh) #84 für Ziel, 78 für Normal, 81 für start und Normal
        #print(dyn_tresh)
        if dyn_tresh < 100:
            dyn_tresh = 150
        ret, self.roiImg = cv2.threshold(self.roiImg, dyn_tresh, 255, 0)

    def erode_dilate(self):
        """
        Eliminate noises
        :return:
        """
        self.roiImg = cv2.erode(self.roiImg, self.erodeElmt)
        self.roiImg = cv2.dilate(self.roiImg, self.dilateElmt)

    def find_contours(self):
        """
        Search for contours
        :return:
        """
        _, self.contours, hierarchy = cv2.findContours(self.roiImg, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)