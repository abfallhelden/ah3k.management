from ah3k_management.line_detection.interface import LineInfo


class LineDetectionDelegate:
    def line_info(self, line_info: LineInfo):
        pass

    def end_line_detected(self):
        """
        Notification about the detection of the end line.
        :param distance: Distance of the end line relative to front of vehicle in [mm].
        """
        pass


class LineDetection:
    def start_tracking(self):
        """
        Start image processing for line detection.
        """
        pass

    def stop_tracking(self):
        """
        Stop image processing
        """
        pass

    def look_for_endline(self):
        """
        Start Endline Detection
        """
        pass

    def set_delegate(self, delegate: LineDetectionDelegate):
        pass
