from ah3k_management.line_detection.LineDetectionImpl import LineDetectionImpl


class LineDetectionFactory(object):
    """
    Factory for creating instance(s) of an implementation of a LineDetection.
    """

    @staticmethod
    def get_line_detection(cam_index, treshold: int, show_video: bool):
        linedetection = LineDetectionImpl(cam_index, treshold, show_video)
        return linedetection
