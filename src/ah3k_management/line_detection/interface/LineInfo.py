class LineInfo(object):
    """
    Distance from the line to the vehicle on three positions. Front, middle and back.
    """
    distanceFront = 0
    """ Distance from front of line to the edge of the car."""

    def __init__(self, front: int):
        """
        Initialize a LineInfo Object
        :param front: Distance from front of line to the edge of the car.
        :return: Line Info Object.
        """
        super().__init__()
        self.distanceFront = front
