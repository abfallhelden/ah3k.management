class Region(object):

    def set_frame(self, frame: object):
        pass

    def create_roi(self):
        pass

    def convert_rgb_gray(self):
        pass

    def threshold_bitwise(self):
        pass

    def erode_dilate(self):
        pass

    def find_contours(self):
        pass

    def search_contours(self):
        pass