# -*- coding: utf-8 -*-
"""Project metadata

Information describing the project.
"""

# The package name, which is also the "UNIX name" for the project.
package = 'ah3k_management'
project = "Abfallheld 3000 Management Software"
project_no_spaces = project.replace(' ', '')
version = '0.0.1'
description = 'Management Software of the autonomous'
authors = ['Nils Furrer', 'Michael Zurmuehle']
authors_string = ', '.join(authors)
emails = ['nils.furrer@stud.hslu.ch', 'michael.zurmuehle@stud.hslu.ch']
license = 'MIT'
copyright = '2015 ' + authors_string
url = 'https://bitbucket.org/abfallhelden/ah3k.management'
