from ah3k_management.operator.Input import Input
from ah3k_management.operator.Resources import Resources


class Action:
    def __init__(self, resources: Resources):
        self.resources = resources

    def run(self, info_input: Input):
        pass