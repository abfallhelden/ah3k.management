import logging

from ah3k_management.line_detection.interface.LineInfo import LineInfo
from ah3k_management.shared.Configuration import Configuration

ChangeLimit = 18
AmountToIgnore = 0


class Calculator:
    def __init__(self, configuration: Configuration):
        self.configuration = configuration
        self.prev_distance = None
        self.ignore_counter = 0

    def steer_radius_for_line_info(self, line_info: LineInfo) -> int:
        optimal_distance = int(self.configuration.optimal_distance)
        k_positive = int(self.configuration.k_constant_right)
        k_negative = int(self.configuration.k_constant_left)

        if self.prev_distance is None or \
                        self.ignore_counter >= AmountToIgnore or \
                                -ChangeLimit < (self.prev_distance - line_info.distanceFront) < ChangeLimit:
            dif = optimal_distance - line_info.distanceFront
            self.prev_distance = line_info.distanceFront
            self.ignore_counter = 0
        else:
            self.ignore_counter += 1
            dif = optimal_distance - self.prev_distance

        if dif == 0:
            radius = 0
            direction = "GERADE"
        else:
            if dif > 0:
                k = k_positive
                direction = "R"
            else:
                k = k_negative
                direction = "L"

            radius = k / dif

        logging.debug("(front:{}, dif:{}, radius:{}  {})".format(
            line_info.distanceFront, dif, radius, direction))

        return int(radius)
