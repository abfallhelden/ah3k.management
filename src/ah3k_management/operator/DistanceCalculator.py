import logging
import threading
from time import perf_counter


class DistanceCalculator:
    def __init__(self, speed: int):
        self.speed = speed
        self.is_running = False
        self.times = []

    def start(self):
        if not self.is_running:
            self.times.append(perf_counter())
            self.is_running = True

    def stop(self):
        if self.is_running:
            self.times.append(perf_counter())
            self.is_running = False

    def get_distance(self) -> int:
        total_runtime = 0
        grouped_times = grouped(self.times, 2)

        if len(self.times) % 2 == 1:
            grouped_times.append((self.times[-1], perf_counter()))

        for start, stop in grouped_times:
            total_runtime += stop - start

        return int(total_runtime * self.speed)

    def reset(self):
        self.is_running = False
        self.times.clear()


def grouped(iterable, n):
    "s -> (s0,s1,s2,...sn-1), (sn,sn+1,sn+2,...s2n-1), (s2n,s2n+1,s2n+2,...s3n-1), ..."
    return list(zip(*[iter(iterable)] * n))


class DistanceNotifier:
    def __init__(self, distance_calculator: DistanceCalculator, required_distance: int, action_on_reached):
        self.required_distance = required_distance
        self.action_on_reached = action_on_reached
        self.distance_calculator = distance_calculator
        self.reached = False
        self.should_stop = False

    def start(self):
        logging.debug("start distance notifier")
        self.should_stop = False
        threading.Timer(1, self._poll).start()

    def stop(self):
        self.should_stop = True

    def _poll(self):
        if not self.reached and self.distance_calculator.get_distance() >= self.required_distance:
            self.reached = True
            self.stop()
            self.action_on_reached()

        if not self.should_stop:
            threading.Timer(1, self._poll).start()
