from datetime import datetime


class Input:
    def __init__(self):
        self.time = datetime.now()

    def to_string(self):
        return str(type(self).__name__)

    @staticmethod
    def input_id() -> int:
        pass
