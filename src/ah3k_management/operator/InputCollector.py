from ah3k_management.container_recognition.interface.ContainerRecognition import ContainerRecognitionDelegate
from ah3k_management.controller_adapter.interface.ControllerAdapter import ControllerAdapterDelegate
from ah3k_management.line_detection.interface.LineDetection import LineDetectionDelegate
from ah3k_management.line_detection.interface.LineInfo import LineInfo
from ah3k_management.operator import InputHandler
from ah3k_management.operator.inputs.BatteryLowInput import BatteryLowInput
from ah3k_management.operator.inputs.ContainerDroppedInput import ContainerDroppedInput
from ah3k_management.operator.inputs.ContainerDetectedInput import ContainerDetectedInput
from ah3k_management.operator.inputs.ContainerNotFoundInput import ContainerNotFoundInput
from ah3k_management.operator.inputs.ControllerReadyInput import ControllerReadyInput
from ah3k_management.operator.inputs.CrossroadCleanInput import CrossroadCleanInput
from ah3k_management.operator.inputs.CrossroadDetectedInput import CrossroadDetectedInput
from ah3k_management.operator.inputs.EndLineInput import EndLineInput
from ah3k_management.operator.inputs.LineInfoInput import LineInfoInput
from ah3k_management.operator.inputs.LoadDroppedInput import LoadDroppedInput
from ah3k_management.operator.inputs.SpeedReachedInput import SpeedReachedInput
from ah3k_management.operator.inputs.StartInput import StartInput
from ah3k_management.shared.ContainerColor import ContainerColor


class InputCollector(ContainerRecognitionDelegate, LineDetectionDelegate, ControllerAdapterDelegate):
    def __init__(self, handler: InputHandler):
        self.handler = handler

    def container_detected(self):
        information_input = ContainerDetectedInput()
        self.handler.add_input(information_input)

    def line_info(self, line_info: LineInfo):
        information_input = LineInfoInput(line_info)
        self.handler.add_input(information_input)

    def crossroad_detected(self):
        information_input = CrossroadDetectedInput()
        self.handler.add_input(information_input)

    def end_line_detected(self):
        information_input = EndLineInput()
        self.handler.add_input(information_input)

    def start(self, container_color: ContainerColor):
        information_input = StartInput(container_color)
        self.handler.add_input(information_input)

    def speed_reached(self):
        information_input = SpeedReachedInput()
        self.handler.add_input(information_input)

    def battery_low(self):
        information_input = BatteryLowInput()
        self.handler.add_input(information_input)

    def load_dropped(self):
        information_input = LoadDroppedInput()
        self.handler.add_input(information_input)

    def container_dropped(self):
        information_input = ContainerDroppedInput()
        self.handler.add_input(information_input)

    def container_not_found(self):
        information_input = ContainerNotFoundInput()
        self.handler.add_input(information_input)

    def ready(self):
        information_input = ControllerReadyInput()
        self.handler.add_input(information_input)

    def crossroad_clean(self):
        information_input = CrossroadCleanInput()
        self.handler.add_input(information_input)
