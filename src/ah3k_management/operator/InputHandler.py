import logging
from threading import Condition

from ah3k_management.operator import Input
from ah3k_management.operator.Resources import Resources
from ah3k_management.operator.Rule import Rule
from ah3k_management.shared.DrivingStatus import DrivingStatus
from ah3k_management.shared.Status import Status


class InputHandler:
    def __init__(self, resources: Resources):
        self.rules = {}
        self.__resources = resources
        self.input_queue = resources.input_queue
        self.handler_lock = Condition()
        self.shall_stop = False

    def status(self):
        return self.__resources.status

    def driving_status(self):
        return self.__resources.driving_status

    def stop(self):
        logging.info('stopping input handler')
        self.handler_lock.acquire()
        self.shall_stop = True
        self.handler_lock.notify_all()
        self.handler_lock.release()

    def run(self):
        logging.info('starting input handler')

        for item in iter(self.input_queue.get, None):
            # Use data
            self.handle_input(item)

    def add_input(self, info_input: Input):
        logging.debug('input was added to queue: %s', info_input.to_string())
        self.input_queue.put(info_input)

    def handle_input(self, next_input):
        distance = self.__resources.distance_calculator.get_distance()
        distance_m = int(distance / 1000)
        distance_cm = int(distance / 10 - distance_m * 100)
        logging.debug('Handling Input: %s (%s, %s) at %dm %dcm', next_input.to_string(), self.status(),
                     self.driving_status(), distance_m, distance_cm)

        # Find the matching Rule
        rule = self._get_matching_rule(next_input)

        # Execute actions of that Rule.
        if rule:
            for action in rule.actions:
                action.run(next_input)
        else:
            logging.warning('No rule found for: %s (%s, %s)', next_input.to_string(), self.status(),
                            self.driving_status())

    def _get_matching_rule(self, info_input) -> Rule:
        # Check for exact Match.
        rule_hash = Rule.hash_of_input_and_status(info_input, self.status(), self.driving_status())
        rule = self.rules.get(rule_hash)

        if not rule:
            # Check with Status 'Any'.
            logging.debug('No rule found for: %s (%s, %s). looking for rule with Status Any', info_input.to_string(),
                          self.status(), self.driving_status())
            rule_hash = Rule.hash_of_input_and_status(info_input, Status.Any, self.driving_status())
            rule = self.rules.get(rule_hash)

            # Check with Status 'Any' and DrivingStatus 'Any'.
            if not rule:
                logging.debug('No rule found for: %s (%s, %s). looking for rule with Status Any and Driving Status Any',
                              info_input.to_string(), Status.Any, self.driving_status())
                rule_hash = Rule.hash_of_input_and_status(info_input, Status.Any, DrivingStatus.Any)
                rule = self.rules.get(rule_hash)

        return rule
