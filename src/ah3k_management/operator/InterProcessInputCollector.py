from multiprocessing import Queue

from ah3k_management.container_recognition.interface.ContainerRecognition import ContainerRecognitionDelegate
from ah3k_management.controller_adapter.interface.ControllerAdapter import ControllerAdapterDelegate
from ah3k_management.line_detection.interface.LineDetection import LineDetectionDelegate
from ah3k_management.line_detection.interface.LineInfo import LineInfo
from ah3k_management.operator.inputs.BatteryLowInput import BatteryLowInput
from ah3k_management.operator.inputs.ContainerDroppedInput import ContainerDroppedInput
from ah3k_management.operator.inputs.ContainerDetectedInput import ContainerDetectedInput
from ah3k_management.operator.inputs.ContainerNotFoundInput import ContainerNotFoundInput
from ah3k_management.operator.inputs.ControllerReadyInput import ControllerReadyInput
from ah3k_management.operator.inputs.CrossroadCleanInput import CrossroadCleanInput
from ah3k_management.operator.inputs.CrossroadDetectedInput import CrossroadDetectedInput
from ah3k_management.operator.inputs.EndLineInput import EndLineInput
from ah3k_management.operator.inputs.LineInfoInput import LineInfoInput
from ah3k_management.operator.inputs.LoadDroppedInput import LoadDroppedInput
from ah3k_management.operator.inputs.SpeedReachedInput import SpeedReachedInput
from ah3k_management.operator.inputs.StartInput import StartInput
from ah3k_management.shared.ContainerColor import ContainerColor


class InterProcessInputCollector(ContainerRecognitionDelegate, LineDetectionDelegate, ControllerAdapterDelegate):
    def __init__(self, queue: Queue):
        self.queue = queue

    def container_detected(self):
        info_input = ContainerDetectedInput()
        self.queue.put(info_input)

    def line_info(self, line_info: LineInfo):
        info_input = LineInfoInput(line_info)
        self.queue.put(info_input)

    def crossroad_detected(self):
        info_input = CrossroadDetectedInput()
        self.queue.put(info_input)

    def end_line_detected(self):
        info_input = EndLineInput()
        self.queue.put(info_input)

    def start(self, container_color: ContainerColor):
        info_input = StartInput(container_color)
        self.queue.put(info_input)

    def speed_reached(self):
        info_input = SpeedReachedInput()
        self.queue.put(info_input)

    def battery_low(self):
        info_input = BatteryLowInput()
        self.queue.put(info_input)

    def load_dropped(self):
        info_input = LoadDroppedInput()
        self.queue.put(info_input)

    def container_dropped(self):
        info_input = ContainerDroppedInput()
        self.queue.put(info_input)

    def container_not_found(self):
        info_input = ContainerNotFoundInput()
        self.queue.put(info_input)

    def ready(self):
        info_input = ControllerReadyInput()
        self.queue.put(info_input)

    def crossroad_clean(self):
        info_input = CrossroadCleanInput()
        self.queue.put(info_input)
