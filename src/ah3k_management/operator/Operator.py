import logging
import threading
from multiprocessing import Queue
from threading import Condition

from ah3k_management.container_recognition.interface.ContainerRecognition import ContainerRecognition
from ah3k_management.controller_adapter.interface.ControllerAdapter import ControllerAdapter
from ah3k_management.operator.DistanceCalculator import DistanceNotifier
from ah3k_management.operator.Input import Input
from ah3k_management.operator.InputCollector import InputCollector
from ah3k_management.operator.InputHandler import InputHandler
from ah3k_management.operator.OperatorNotification import OperatorNotification
from ah3k_management.operator.Resources import Resources
from ah3k_management.operator.RuleDefinition import RuleDefinition
from ah3k_management.operator.actions.SetStatusAction import SetStatusAction
from ah3k_management.operator.inputs.DistanceReachedInput import DistanceReachedInput
from ah3k_management.operator.inputs.ManagementReadyInput import ManagementReadyInput
from ah3k_management.processing.container_recognition import ContainerRecognitionProcessSender
from ah3k_management.processing.line_detection import LineDetectionProcessSender
from ah3k_management.shared import Configuration
from ah3k_management.shared.Status import Status


class Operator(OperatorNotification):
    def __init__(self, controller: ControllerAdapter, line_detection: LineDetectionProcessSender,
                 container_recognition: ContainerRecognitionProcessSender, config: Configuration, input_queue: Queue):
        self.shall_shutdown = False
        self.loop_lock = Condition()
        self.resources = Resources(input_queue=input_queue, config=config, line_detection=line_detection)
        self.input_handler = InputHandler(self.resources)
        self.input_collector = InputCollector(self.input_handler)
        self.line_detection_sender = line_detection
        self.container_recognition_sender = container_recognition

        self.resources.notification = self
        self.resources.controller_adapter = controller
        self.resources.controller_adapter.set_delegate(self.input_collector)
        self.resources.container_recognition = container_recognition
        self.resources.configuration = config

    def run(self):
        # Send Keepalives
        logging.debug("sending 3 keepalive messages")
        for i in range(0, 3):
            self.resources.controller_adapter.keep_alive()

        # Check Interface version of Controller
        controller_version = self.resources.controller_adapter.get_interface_version()
        if controller_version != self.resources.controller_adapter.version:
            logging.error("mismatching interface version. Management: %s, Controller: %s",
                          self.resources.controller_adapter.version, controller_version)
            return

        # Build up Rules
        logging.debug("biulding up rules")
        self.input_handler.rules = RuleDefinition(self.resources).rules

        # Start InputHandler
        logging.info("starting thread OperatorInputHandler")
        handler_thread = threading.Thread(target=self.input_handler.run, name="OperatorInputHandler")
        handler_thread.start()

        # Initialize Controller
        logging.info("starting initialization")
        status_action = SetStatusAction(self.resources, Status.Initializing)
        status_action.run(Input())
        self.resources.controller_adapter.init()

        # Initialize Line Detection
        self.line_detection_sender.init()

        # Initialize Container Recognition
        self.container_recognition_sender.init()

        # Initialize Distance Notifier
        self.resources.distance_notifier = DistanceNotifier(self.resources.distance_calculator, 12000,
                                                            self.distance_for_endline_stuff)

        # Throw ManagementReady Input
        logging.debug("management ready")
        self.input_handler.add_input(ManagementReadyInput())

        # Endless Loop until shutdown
        self.loop_lock.acquire()
        while not self.shall_shutdown:
            self.loop_lock.wait()
        self.loop_lock.release()
        logging.info("end of operator")

    def shutdown(self):
        logging.info("received shutdown command")
        self.loop_lock.acquire()
        self.shall_shutdown = True
        self.line_detection_sender.kill()
        self.input_handler.stop()
        self.loop_lock.notify_all()
        self.loop_lock.release()
        print("finished shutdown")

    def distance_for_endline_stuff(self):
        self.resources.input_queue.put(DistanceReachedInput())
