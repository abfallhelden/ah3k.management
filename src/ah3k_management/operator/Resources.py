from multiprocessing import Queue

from ah3k_management.container_recognition.interface.ContainerRecognition import ContainerRecognition
from ah3k_management.controller_adapter.interface.ControllerAdapter import ControllerAdapter
from ah3k_management.line_detection.interface.LineDetection import LineDetection
from ah3k_management.operator.Calculator import Calculator
from ah3k_management.operator.DistanceCalculator import DistanceCalculator, DistanceNotifier
from ah3k_management.operator.OperatorNotification import OperatorNotification
from ah3k_management.shared.Configuration import Configuration
from ah3k_management.shared.ContainerColor import ContainerColor
from ah3k_management.shared.DrivingStatus import DrivingStatus
from ah3k_management.shared.Status import Status


class Resources:
    def __init__(self, input_queue: Queue, notification: OperatorNotification = None,
                 controller_adapter: ControllerAdapter = None, line_detection: LineDetection = None,
                 container_recognition: ContainerRecognition = None, config: Configuration = None):
        self.container_recognition = container_recognition
        self.line_detection = line_detection
        self.configuration = config
        self.current_speed = 0
        self.current_radius = 0
        self.input_queue = input_queue
        self.controller_adapter = controller_adapter
        self.notification = notification
        self.status = Status.Start
        self.driving_status = DrivingStatus.Standing
        self.container_color = ContainerColor.Undefined
        self.calculator = Calculator(self.configuration)
        self.distance_calculator = DistanceCalculator(int(self.configuration.default_speed))
        self.distance_notifier = None
