from ah3k_management.operator import Action
from ah3k_management.shared.DrivingStatus import DrivingStatus
from ah3k_management.shared.Status import Status


class Rule:
    def __init__(self, input_type, status: Status, driving_status: DrivingStatus, actions: [Action]):
        self.driving_status = driving_status
        self.input_type = input_type
        self.status = status
        self.actions = actions

    def add_action(self, action: Action):
        self.actions.append(action)

    def hash(self):
        return Rule.hash_of_input_and_status(self.input_type, self.status, self.driving_status)

    @staticmethod
    def hash_of_input_and_status(input_type, status: Status, driving_status: DrivingStatus):
        return int(driving_status.value)*512 + int(status.value) * 32 + input_type.input_id()