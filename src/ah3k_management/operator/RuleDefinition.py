from ah3k_management.operator import Resources
from ah3k_management.operator.Rule import Rule
from ah3k_management.operator.actions.DropLoadAction import DropLoadAction
from ah3k_management.operator.actions.IgnoreAction import IgnoreAction
from ah3k_management.operator.actions.LookForEndLineAction import LookForEndLineAction
from ah3k_management.operator.actions.ManualSteerAction import ManualSteerAction
from ah3k_management.operator.actions.PlaySoundAction import PlaySoundAction, Sounds
from ah3k_management.operator.actions.SetDrivingStatusAction import SetDrivingStatusAction
from ah3k_management.operator.actions.SetStatusAction import SetStatusAction
from ah3k_management.operator.actions.ShutdownAction import ShutdownAction
from ah3k_management.operator.actions.SpeedAction import SpeedAction
from ah3k_management.operator.actions.StartContainerRecognitionAction import StartContainerRecognitionAction
from ah3k_management.operator.actions.StartLineDetectionAction import StartLineDetectionAction
from ah3k_management.operator.actions.SteerAction import SteerAction
from ah3k_management.operator.actions.StopContainerRecognitionAction import StopContainerRecognitionAction
from ah3k_management.operator.actions.StopForContainerAndGrabAction import StopForContainerAndGrabAction
from ah3k_management.operator.actions.StopAfterDistanceAction import StopAfterDistanceAction
from ah3k_management.operator.actions.StopLineDetectionAction import StopLineDetectionAction
from ah3k_management.operator.actions.StoreContainerColorAction import StoreContainerColorAction
from ah3k_management.operator.actions.WaitForCleanCrossroadAction import WaitForCleanCrossroadAction
from ah3k_management.operator.inputs.BatteryLowInput import BatteryLowInput
from ah3k_management.operator.inputs.ContainerDroppedInput import ContainerDroppedInput
from ah3k_management.operator.inputs.ContainerDetectedInput import ContainerDetectedInput
from ah3k_management.operator.inputs.ContainerNotFoundInput import ContainerNotFoundInput
from ah3k_management.operator.inputs.ControllerReadyInput import ControllerReadyInput
from ah3k_management.operator.inputs.CrossroadCleanInput import CrossroadCleanInput
from ah3k_management.operator.inputs.CrossroadDetectedInput import CrossroadDetectedInput
from ah3k_management.operator.inputs.DistanceReachedInput import DistanceReachedInput
from ah3k_management.operator.inputs.EndLineInput import EndLineInput
from ah3k_management.operator.inputs.LineInfoInput import LineInfoInput
from ah3k_management.operator.inputs.LoadDroppedInput import LoadDroppedInput
from ah3k_management.operator.inputs.ManagementReadyInput import ManagementReadyInput
from ah3k_management.operator.inputs.SpeedReachedInput import SpeedReachedInput
from ah3k_management.operator.inputs.StartInput import StartInput
from ah3k_management.shared.DrivingStatus import DrivingStatus
from ah3k_management.shared.Status import Status


class RuleDefinition:
    def __init__(self, resources: Resources):
        self.resources = resources
        self.rules = {}

        self.init_rules(resources)

        self.start_drive_and_line_tracking_rules(resources)

        self.container_recognition_rules(resources)

        self.crossroad_rules(resources)

        self.end_line_rules(resources)

        self.battery_low_rules(resources)

    def add_rule(self, rule: Rule):
        self.rules[rule.hash()] = rule

    def init_rules(self, resources: Resources):
        # Set Ready on Controller Ready
        self.add_rule(Rule(ControllerReadyInput, Status.Initializing, DrivingStatus.Standing,
                           [SetStatusAction(resources, Status.ControllerReady)]))
        self.add_rule(Rule(ControllerReadyInput, Status.ManagementReady, DrivingStatus.Standing,
                           [SetStatusAction(self.resources, Status.Ready),
                            PlaySoundAction(self.resources, Sounds.Ready)]))

        # Set Ready on Management Ready
        self.add_rule(Rule(ManagementReadyInput, Status.Initializing, DrivingStatus.Standing,
                           [SetStatusAction(resources, Status.ManagementReady),
                            #StartLineDetectionAction(resources)
                            ]))
        self.add_rule(Rule(ManagementReadyInput, Status.ControllerReady, DrivingStatus.Standing,
                           [SetStatusAction(self.resources, Status.Ready),
                            #StartLineDetectionAction(resources),
                            PlaySoundAction(self.resources, Sounds.Ready)]))

        self.add_rule(Rule(LineInfoInput, Status.Ready, DrivingStatus.Standing,
                           [IgnoreAction(resources)]))

    def start_drive_and_line_tracking_rules(self, resources):
        # Start Drive and Linetracking
        self.add_rule(Rule(StartInput, Status.Ready, DrivingStatus.Standing,
                           [SetStatusAction(self.resources, Status.Driving),
                            SetDrivingStatusAction(self.resources, DrivingStatus.Accelerating),
                            StoreContainerColorAction(self.resources),
                            StartContainerRecognitionAction(self.resources),
                            StartLineDetectionAction(self.resources),
                            SpeedAction(self.resources, self.resources.configuration.default_speed),
                            ]))

        # Speed was reached.
        self.add_rule(Rule(SpeedReachedInput, Status.Driving, DrivingStatus.Accelerating,
                           [SetDrivingStatusAction(self.resources, DrivingStatus.Moving)]))

        # Camera recognized a line, do steering.
        self.add_rule(Rule(LineInfoInput, Status.Driving, DrivingStatus.Accelerating,
                           [SteerAction(resources)]))
        self.add_rule(Rule(LineInfoInput, Status.Driving, DrivingStatus.Moving,
                           [SteerAction(resources)]))

    def container_recognition_rules(self, resources: Resources):
        # Container was recognized. Send message to controller.
        self.add_rule(Rule(ContainerDetectedInput, Status.Driving, DrivingStatus.Moving,
                           [SetStatusAction(resources, Status.ReachingContainer),
                            SetDrivingStatusAction(resources, DrivingStatus.Stopping),
                            #SpeedAction(resources, 50),
                            StopForContainerAndGrabAction(resources),
                            PlaySoundAction(resources, Sounds.Blip)]))

        # Still need to steer across line.
        self.add_rule(Rule(LineInfoInput, Status.ReachingContainer, DrivingStatus.Stopping,
                           [SteerAction(resources)]))

        # In case the controller did not find a container, go back to normal.
        self.add_rule(Rule(ContainerNotFoundInput, Status.ReachingContainer, DrivingStatus.Stopping,
                           [SpeedAction(resources, speed=self.resources.configuration.default_speed),
                            SetDrivingStatusAction(resources, DrivingStatus.Accelerating),
                            SetStatusAction(resources, Status.Driving)]))

        # Controller found container and stopped next to it.
        self.add_rule(Rule(SpeedReachedInput, Status.ReachingContainer, DrivingStatus.Stopping,
                           [SetDrivingStatusAction(resources, DrivingStatus.Standing),
                            SetStatusAction(resources, Status.EmptyingContainer),
                            ]))
        self.add_rule(Rule(LineInfoInput, Status.EmptyingContainer, DrivingStatus.Standing,
                           [IgnoreAction(resources)]))
        self.add_rule(Rule(ContainerDetectedInput, Status.ReachingContainer, DrivingStatus.Stopping,
                           [IgnoreAction(resources)]))
        self.add_rule(Rule(ContainerDetectedInput, Status.EmptyingContainer, DrivingStatus.Standing,
                           [IgnoreAction(resources)]))

        # Whole container-emptying procedure is finished. Go back to normal.
        self.add_rule(Rule(ContainerDroppedInput, Status.EmptyingContainer, DrivingStatus.Standing,
                           [SetStatusAction(resources, Status.Driving),
                            SpeedAction(resources, self.resources.configuration.default_speed),
                            SetDrivingStatusAction(resources, DrivingStatus.Accelerating)]))

    def crossroad_rules(self, resources):
        # Crossroad was detected, stop.
        self.add_rule(Rule(CrossroadDetectedInput, Status.Driving, DrivingStatus.Moving,
                           [SetStatusAction(resources, Status.WaitingForPrecedence),
                            SetDrivingStatusAction(resources, DrivingStatus.Stopping),
                            #SpeedAction(resources, 0),
                            StopAfterDistanceAction(resources, 80),
                            PlaySoundAction(resources, Sounds.Honk),
                            ]))
        # Temporary for Testing:
        """self.add_rule(Rule(SpeedReachedInput, Status.WaitingForPrecedence, DrivingStatus.Stopping,
                           [SetStatusAction(resources, Status.Driving),
                            SetDrivingStatusAction(resources, DrivingStatus.Accelerating),
                            SpeedAction(resources, self.resources.configuration.default_speed),
                            ]))
                            """

        # Car stopped, wait until it is safe to go.
        self.add_rule(Rule(SpeedReachedInput, Status.WaitingForPrecedence, DrivingStatus.Stopping,
                           [SetDrivingStatusAction(resources, DrivingStatus.Standing),
                            WaitForCleanCrossroadAction(resources)]))

        # Crossroad is safe, back to normal.
        self.add_rule(Rule(CrossroadCleanInput, Status.WaitingForPrecedence, DrivingStatus.Standing,
                           [SetStatusAction(resources, Status.Driving),
                            SetDrivingStatusAction(resources, DrivingStatus.Accelerating),
                            SpeedAction(resources, self.resources.configuration.default_speed)]))

    def end_line_rules(self, resources):
        # Tell line detection to look for endline after a specific distance
        self.add_rule(Rule(DistanceReachedInput, Status.Driving, DrivingStatus.Moving,
                           [LookForEndLineAction(resources)]))
        self.add_rule(Rule(DistanceReachedInput, Status.ReachingContainer, DrivingStatus.Stopping,
                           [LookForEndLineAction(resources)]))
        self.add_rule(Rule(DistanceReachedInput, Status.EmptyingContainer, DrivingStatus.Standing,
                           [LookForEndLineAction(resources)]))

        # EndLine detected, stop
        self.add_rule(Rule(EndLineInput, Status.Driving, DrivingStatus.Moving,
                           [SetStatusAction(resources, Status.ReachingEndLine),
                            SpeedAction(resources, 100),
                            #ManualSteerAction(resources, 0),
                            StopAfterDistanceAction(resources, 150),
                            #StopAfterDistanceAction(resources, 380),
                            StopContainerRecognitionAction(resources),
                            PlaySoundAction(resources, Sounds.Endline_detected)]))

        # don't need to steer across line.
        self.add_rule(Rule(LineInfoInput, Status.ReachingEndLine, DrivingStatus.Moving,
                           [SteerAction(resources)]))
        self.add_rule(Rule(LineInfoInput, Status.ReachingEndLine, DrivingStatus.Stopping,
                           [SteerAction(resources)]))
        self.add_rule(Rule(EndLineInput, Status.ReachingEndLine, DrivingStatus.Moving,
                           [IgnoreAction(resources)]))

        # Car reached the slower speed
        self.add_rule(Rule(SpeedReachedInput, Status.ReachingEndLine, DrivingStatus.Moving,
                            [SetDrivingStatusAction(resources, DrivingStatus.Stopping)]))

        # Car stopped in final position. Empty load now.
        self.add_rule(Rule(SpeedReachedInput, Status.ReachingEndLine, DrivingStatus.Stopping,
                           [SetStatusAction(resources, Status.EmptyingLoad),
                            SetDrivingStatusAction(resources, DrivingStatus.Standing),
                            DropLoadAction(resources),
                            StopLineDetectionAction(resources),
                            PlaySoundAction(resources, Sounds.Emptying_load)]))
        self.add_rule(Rule(LineInfoInput, Status.EmptyingLoad, DrivingStatus.Standing, [IgnoreAction(resources)]))
        self.add_rule(Rule(ContainerDetectedInput, Status.EmptyingLoad, DrivingStatus.Standing, [IgnoreAction(resources)]))

        # Load was dropped, Procedure ended.
        self.add_rule(Rule(LoadDroppedInput, Status.EmptyingLoad, DrivingStatus.Standing,
                           [SetStatusAction(resources, Status.End),
                            PlaySoundAction(resources, Sounds.Thanks)]))
        self.add_rule(Rule(LineInfoInput, Status.End, DrivingStatus.Standing,
                           [IgnoreAction(resources)]))
        self.add_rule(Rule(ContainerDetectedInput, Status.End, DrivingStatus.Standing,
                           [IgnoreAction(resources)]))

    def battery_low_rules(self, resources):
        # Battery Low
        self.add_rule(Rule(BatteryLowInput, Status.Any, DrivingStatus.Accelerating,
                           [SetStatusAction(resources, Status.Error),
                            SetDrivingStatusAction(resources, DrivingStatus.Stopping),
                            SpeedAction(resources, 0)]))
        self.add_rule(Rule(BatteryLowInput, Status.Any, DrivingStatus.Moving,
                           [SetStatusAction(resources, Status.Error),
                            SetDrivingStatusAction(resources, DrivingStatus.Stopping),
                            SpeedAction(resources, 0)]))
        self.add_rule(Rule(BatteryLowInput, Status.Any, DrivingStatus.Any,
                           [SetStatusAction(resources, Status.Error),
                            ShutdownAction(resources)]))
        self.add_rule(Rule(SpeedReachedInput, Status.Error, DrivingStatus.Stopping,
                           [ShutdownAction(resources)]))
