import logging

from ah3k_management.operator.Action import Action
from ah3k_management.operator.Input import Input


class LookForEndLineAction(Action):
    def run(self, info_input: Input):
        logging.debug("running LookForEndLineAction")
        self.resources.line_detection.look_for_endline()