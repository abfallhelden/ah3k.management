from ah3k_management.operator.Action import Action
from ah3k_management.operator.Input import Input
from ah3k_management.operator.Resources import Resources


class ManualSteerAction(Action):
    def __init__(self, resources: Resources, radius: int):
        super().__init__(resources)
        self.radius = radius

    def run(self, info_input: Input):
        self.resources.controller_adapter.steer(self.radius)
