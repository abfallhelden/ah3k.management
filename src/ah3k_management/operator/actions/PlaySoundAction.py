import logging
from enum import Enum

from subprocess import Popen

from ah3k_management.operator.Action import Action
from ah3k_management.operator.Input import Input
from ah3k_management.operator.Resources import Resources


class Sounds(Enum):
    Blip = "blip1.wav"
    Start = "start.wav"
    Green_Container = "green.wav"
    Blue_Container = "blue.wav"
    Endline_detected = "endline.wav"
    Crossroad = "crossroad.wav"
    Emptying_load = "emptying.wav"
    Thanks = "thanks.wav"
    Honk = "car_honk.wav"
    Break = "car_break.wav"
    Ready = "ready.wav"
    Accelerate = "car_accelerate.wav"
    Container_found = "container_found.wav"


class PlaySoundAction(Action):
    def __init__(self, resources: Resources, sound: Sounds):
        super().__init__(resources)
        self.sound = sound
        self.tmpcounter = 0

    def run(self, info_input: Input):
        try:
            Popen(["aplay", "/home/pi/Music/{}".format(self.sound.value)])
        except:
            logging.error("Unable to play sound (aplay not found?)")
