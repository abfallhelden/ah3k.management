import logging

from ah3k_management.operator.Action import Action
from ah3k_management.operator.Input import Input
from ah3k_management.operator.Resources import Resources
from ah3k_management.shared.DrivingStatus import DrivingStatus


class SetDrivingStatusAction(Action):
    def __init__(self, resources:Resources, driving_status:DrivingStatus):
        super().__init__(resources)
        self.driving_status = driving_status

    def run(self, info_input: Input):
        logging.info("Running Action %s with driving_status: %s", type(self).__name__, self.driving_status)
        self.resources.driving_status = self.driving_status

        # distance measurement
        if self.driving_status == DrivingStatus.Standing:
            self.resources.distance_calculator.stop()
        elif self.driving_status == DrivingStatus.Moving:
            self.resources.distance_calculator.start()
            self.resources.distance_notifier.start()
