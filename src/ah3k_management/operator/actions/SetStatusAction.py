import logging

from ah3k_management.operator.Action import Action
from ah3k_management.operator.Input import Input
from ah3k_management.operator.Resources import Resources
from ah3k_management.shared.Status import Status


class SetStatusAction(Action):
    def __init__(self, resources:Resources, status: Status):
        super().__init__(resources)
        self.status = status

    def run(self, info_input: Input):
        logging.info("Running Action %s with status: %s", type(self).__name__, self.status)
        self.resources.status = self.status
        if self.status != Status.ManagementReady and self.status != Status.ControllerReady:
            self.resources.controller_adapter.set_status(self.status)
