from ah3k_management.operator.Action import Action
from ah3k_management.operator.Input import Input


class ShutdownAction(Action):

    def run(self, info_input: Input):
        # Stop ControllerAdapter
        self.resources.controller_adapter.stop_listener()

        # Stop LineDetection
        self.resources.line_detection.stop_tracking()

        # Stop Container Recognition
        self.resources.container_recognition.stop_tracking()

        # send notification to Operator
        print("will call shutdown on operator")
        self.resources.notification.shutdown()