from ah3k_management.operator.Action import Action
from ah3k_management.operator.Input import Input


class SpeedAction(Action):
    def __init__(self, resources, speed: int):
        super().__init__(resources)
        self.speed = speed

    def run(self, info_input: Input):
        self.resources.current_speed = self.speed
        self.resources.controller_adapter.speed(self.speed)


