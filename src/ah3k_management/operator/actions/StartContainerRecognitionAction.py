from ah3k_management.operator.Action import Action
from ah3k_management.operator.Input import Input


class StartContainerRecognitionAction(Action):
    def run(self, info_input: Input):
        self.resources.container_recognition.start_tracking(self.resources.container_color)