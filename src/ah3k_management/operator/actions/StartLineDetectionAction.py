from ah3k_management.operator.Action import Action
from ah3k_management.operator.Input import Input


class StartLineDetectionAction(Action):
    def run(self, info_input: Input):
        self.resources.line_detection.start_tracking()