import logging

from ah3k_management.operator.Action import Action
from ah3k_management.operator.Input import Input


class SteerAction(Action):
    def run(self, info_input: Input):
        """
        :type info_input: LineInfoInput
        """
        if hasattr(info_input, "line_info"):
            line_info = info_input.line_info
            try:
                radius = self.resources.calculator.steer_radius_for_line_info(line_info)
            except:
                logging.error("Error in Calculator!")
                radius = 0
            if self.resources.current_radius != radius:
                self.resources.controller_adapter.steer(radius)
                self.resources.current_radius = radius