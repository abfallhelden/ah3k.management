import time

from ah3k_management.operator.Action import Action
from ah3k_management.operator.Resources import Resources
from ah3k_management.operator.inputs.EndLineInput import EndLineInput


class StopAfterDistanceAction(Action):
    def __init__(self, resources: Resources, distance: int):
        super().__init__(resources)
        self.distance = distance

    def run(self, info_input: EndLineInput):
        time.sleep(0.1)
        self.resources.controller_adapter.stop_after(self.distance)
