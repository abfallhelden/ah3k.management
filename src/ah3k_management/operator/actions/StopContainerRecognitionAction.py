from ah3k_management.operator.Action import Action
from ah3k_management.operator.Input import Input


class StopContainerRecognitionAction(Action):
    def run(self, info_input: Input):
        self.resources.container_recognition.stop_tracking()