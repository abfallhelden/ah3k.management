import time

from ah3k_management.operator.Action import Action
from ah3k_management.operator.inputs.ContainerDetectedInput import ContainerDetectedInput


class StopForContainerAndGrabAction(Action):
    def run(self, info_input: ContainerDetectedInput):
        time.sleep(0.01)
        self.resources.controller_adapter.stop_for_container_and_grab(200)
