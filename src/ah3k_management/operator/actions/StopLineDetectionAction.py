from ah3k_management.operator.Action import Action
from ah3k_management.operator.Input import Input


class StopLineDetectionAction(Action):

    def run(self, info_input: Input):
        self.resources.line_detection.stop_tracking()