import logging

from ah3k_management.operator.Action import Action
from ah3k_management.operator.Input import Input


class StoreContainerColorAction(Action):
    def run(self, info_input: Input):
        if hasattr(info_input, "container_color"):
            self.resources.container_color = info_input.container_color
        else:
            logging.error("no container color was given")
