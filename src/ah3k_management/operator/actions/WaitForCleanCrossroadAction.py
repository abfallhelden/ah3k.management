from ah3k_management.operator.Action import Action
from ah3k_management.operator.Input import Input


class WaitForCleanCrossroadAction(Action):
    def run(self, info_input: Input):
        self.resources.controller_adapter.wait_for_clean_crossroad()