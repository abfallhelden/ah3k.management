from ah3k_management.operator.Input import Input


class ContainerDroppedInput(Input):
    def __init__(self):
        super().__init__()

    @staticmethod
    def input_id() -> int:
        return 1