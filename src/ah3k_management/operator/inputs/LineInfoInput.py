from ah3k_management.line_detection.interface.LineInfo import LineInfo
from ah3k_management.operator.Input import Input


class LineInfoInput(Input):
    def __init__(self, line_info:LineInfo):
        super().__init__()
        self.line_info = line_info

    @staticmethod
    def input_id() -> int:
        return 8
    