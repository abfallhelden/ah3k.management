import logging

from ah3k_management.operator.Input import Input
from ah3k_management.shared.ContainerColor import ContainerColor


class StartInput(Input):
    def __init__(self, container_color:ContainerColor):
        super().__init__()
        self.container_color = container_color
        logging.info("StartInput with color: {}".format(container_color))
        print(container_color)

    @staticmethod
    def input_id() -> int:
        return 14
