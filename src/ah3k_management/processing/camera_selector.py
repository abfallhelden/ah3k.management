import logging
import subprocess


def get_cam_with_id(hw_id):
    try:
        video0_id = str(subprocess.check_output("udevadm info /dev/video0 | grep \"E: ID_SERIAL_SHORT\"", shell=True))
    except:
        video0_id = ""
    try:
        video1_id = str(subprocess.check_output("udevadm info /dev/video1 | grep \"E: ID_SERIAL_SHORT\"", shell=True))
    except:
        video1_id = ""

    if hw_id in video0_id:
        logging.info("id of video0 is %s", hw_id)
        return 0
    elif hw_id in video1_id:
        logging.info("id of video1 is %s", hw_id)
        return 1
    else:
        logging.error("could not find camera with ID %s, (video0: %s, video1: %s)", hw_id, video0_id, video1_id)
        return None
