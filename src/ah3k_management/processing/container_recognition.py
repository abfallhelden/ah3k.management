import logging
from multiprocessing import Queue, Pipe
from multiprocessing.connection import Connection
from multiprocessing.context import Process

from ah3k_management.container_recognition.interface.ContainerRecognition import ContainerRecognition
from ah3k_management.container_recognition.interface.ContainerRecognitionFactory import ContainerRecognitionFactory
from ah3k_management.operator.InterProcessInputCollector import InterProcessInputCollector
from ah3k_management.shared.ContainerColor import ContainerColor


class ContainerRecognitionProcessSender(ContainerRecognition):
    def __init__(self, pipe: Connection, process: Process):
        self.process = process
        self.pipe = pipe

    def init(self):
        self.pipe.send("init")

    def start_tracking(self, color: ContainerColor):
        self.pipe.send(["start_tracking", color])

    def stop_tracking(self):
        self.pipe.send("stop_tracking")

    def kill(self):
        self.pipe.send("kill")
        print("wait for join")
        self.process.join(10)
        print("joined")
        self.process.terminate()


class ContainerRecognitionProcessListener:
    def __init__(self, pipe: Connection, collector, showvideo: bool):
        self.showvideo = showvideo
        self.collector = collector
        self.pipe = pipe
        self.container_recognition = None
        self.should_listen = False

    def listen(self):
        self.should_listen = True
        while self.should_listen:
            self.handle(self.pipe.recv())
        self.pipe.close()

    def stop(self):
        self.should_listen = False

    def handle(self, message):
        if message == "init":
            self.container_recognition = ContainerRecognitionFactory.get_container_recognition(self.showvideo)
            self.container_recognition.set_delegate(self.collector)
        if message[0] == "start_tracking":
            self.container_recognition.start_tracking(message[1])
        elif message == "stop_tracking":
            self.container_recognition.stop_tracking()
        elif message == "kill":
            print("stopping")
            self.container_recognition.stop_tracking()
            self.stop()


def start_container_recognition_process(queue: Queue, showvideo: bool) -> ContainerRecognitionProcessSender:
    parent_conn, child_conn = Pipe()

    p = Process(target=_run, args=(queue, child_conn, showvideo), name="Container_Recognition_Process")
    p.start()

    return ContainerRecognitionProcessSender(parent_conn, p)


def _run(queue: Queue, pipe: Connection, showvideo: bool):
    collector = InterProcessInputCollector(queue)

    listener = ContainerRecognitionProcessListener(pipe, collector, showvideo)
    listener.listen()

    print("end")