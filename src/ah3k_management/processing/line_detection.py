from multiprocessing import Queue, Pipe
from multiprocessing.connection import Connection
from multiprocessing.context import Process

from ah3k_management.line_detection.interface.LineDetection import LineDetection
from ah3k_management.line_detection.interface.LineDetectionFactory import LineDetectionFactory
from ah3k_management.operator.InterProcessInputCollector import InterProcessInputCollector


class LineDetectionProcessSender(LineDetection):
    def __init__(self, pipe: Connection, process: Process):
        self.process = process
        self.pipe = pipe

    def init(self):
        self.pipe.send("init")

    def start_tracking(self):
        self.pipe.send("start_tracking")

    def look_for_endline(self):
        self.pipe.send("look_for_endline")

    def stop_tracking(self):
        self.pipe.send("stop_tracking")

    def kill(self):
        self.pipe.send("kill")
        print("wait for join")
        self.process.join(10)
        print("joined")
        self.process.terminate()


class LineDetectionProcessListener:
    def __init__(self, pipe: Connection, collector, cam_index, treshold: int, showvideo: bool):
        self.showvideo = showvideo
        self.cam_index = cam_index
        self.treshold = treshold
        self.collector = collector
        self.pipe = pipe
        self.line_detection = None
        self.should_listen = False

    def listen(self):
        self.should_listen = True
        while self.should_listen:
            self.handle(self.pipe.recv())
        self.pipe.close()

    def stop(self):
        self.should_listen = False

    def handle(self, message):
        if message == "init":
            self.line_detection = LineDetectionFactory.get_line_detection(self.cam_index, self.treshold, self.showvideo)
            self.line_detection.set_delegate(self.collector)
        if message == "start_tracking":
            self.line_detection.start_tracking()
        elif message == "look_for_endline":
            self.line_detection.look_for_endline()
        elif message == "stop_tracking":
            self.line_detection.stop_tracking()
        elif message == "kill":
            print("stopping")
            self.line_detection.stop_tracking()
            self.stop()


def start_line_detection_process(queue: Queue, cam_index, treshold: int, showvideo: bool) -> LineDetectionProcessSender:
    parent_conn, child_conn = Pipe()

    p = Process(target=_run, args=(queue, child_conn, cam_index, treshold, showvideo), name="Line_Detection_Process")
    p.start()

    return LineDetectionProcessSender(parent_conn, p)


def _run(queue: Queue, pipe: Connection, cam_index, treshold: int, showvideo: bool):
    collector = InterProcessInputCollector(queue)

    listener = LineDetectionProcessListener(pipe, collector, cam_index, treshold, showvideo)
    listener.listen()

    print("end")
