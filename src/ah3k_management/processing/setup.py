import logging
from multiprocessing import Queue
from subprocess import call

import ah3k_management.processing.container_recognition
import ah3k_management.processing.line_detection
from ah3k_management import processing
from ah3k_management.container_recognition.interface.ContainerRecognition import ContainerRecognition
from ah3k_management.controller_adapter.Communicator import FreedomBoardCommunicator, \
    CommandLineCommunicator
from ah3k_management.controller_adapter.ControllerAdapterImpl import ControllerAdapterImpl
from ah3k_management.line_detection.interface.LineDetection import LineDetection
from ah3k_management.operator.Operator import Operator
from ah3k_management.processing import camera_selector
from ah3k_management.shared.Configuration import Configuration
from ah3k_management.user_interaction.UserConsole import UserConsole

default_config_path = "/home/pi/ah3k.config"


def setup_logging(config: Configuration):
    logging.basicConfig(
        format='%(levelname)s;\t%(asctime)s; %(processName)s; %(threadName)s;\t%(filename)s;\t%(funcName)s:\t%(message)s',
        filename=config.log_path, level=config.log_level)


def run(args: [str]):
    if any(item.startswith('config=') for item in args):
        config_arg = [s for s in args if s.startswith("config=")][0]
        config_path = config_arg.split("=")[1]
        config = Configuration(config_path)
    else:
        config = Configuration(default_config_path)

    setup_logging(config)

    # Parameters
    logging.info("started process with args %s", str(args))
    mock_controller = ("nocontroller" in args) or ("debug" in args)
    user_interaction_mode = "interaction" in args
    nocam = "nocam" in args
    showvideo = "showvideo" in args
    noline = "noline" in args
    nocontainer = "nocontainer" in args

    # Build up ControllerAdapter
    if mock_controller:
        logging.info('using mocked controller')
        communicator = CommandLineCommunicator()
    else:
        communicator = FreedomBoardCommunicator(FreedomBoardCommunicator.freedom_board_port())

    controller_adapter = ControllerAdapterImpl(communicator)

    # Create Line Detection
    queue = Queue()
    if nocam:
        line_detection_cam_index = "Testfahrt.mov"
    else:
        set_up_camera_exposure(config.line_camera_exposure_time)
        line_detection_cam_index = camera_selector.get_cam_with_id(config.line_detection_cam_id)
    line_detection = processing.line_detection.start_line_detection_process(queue, line_detection_cam_index,
                                                                            config.line_camera_treshold, showvideo)
    # Create Container Recognition Processes
    container_recognition = processing.container_recognition.start_container_recognition_process(queue, showvideo)

    # noinspection PyTypeChecker
    operator = Operator(controller=controller_adapter, line_detection=line_detection,
                        container_recognition=container_recognition,
                        config=config, input_queue=queue)

    if user_interaction_mode:
        logging.info('starting user interaction mode')
        user_console = UserConsole(operator)
        user_console.run()
    else:
        logging.info('starting operator')
        operator.run()


def set_up_camera_exposure(exposure_time: int):
    command = ["v4l2-ctl", "-d", "/dev/video0", "--set-ctrl", "exposure_absolute={}".format(exposure_time)]
    call(command)
