import cv2

from ah3k_management.container_recognition.ContainerFrameAnalyzer import ContainerFrameAnalyzer
from ah3k_management.operator.actions.PlaySoundAction import PlaySoundAction, Sounds
from ah3k_management.shared.ContainerColor import ContainerColor

analyzer = ContainerFrameAnalyzer(ContainerColor.Blue, True)
player = PlaySoundAction(None, Sounds.Blip)

RASPI = True

if RASPI:
    from picamera import PiCamera
    from picamera.array import PiRGBArray


    camera = PiCamera()
    camera.resolution = (640, 480)
    camera.framerate = 4
    rawCapture = PiRGBArray(camera, size=(640, 480))

    for the_frame in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True):
        frame = cv2.flip(the_frame.array, -1)
        cv2.imshow("original", frame)


        if analyzer.has_container(frame):
            player.run(None)
            print("FOUND!")

        rawCapture.truncate(0)

        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

else:
    cap = cv2.VideoCapture(0)

    cap.set(3, 1280 * 0.5)  # width)
    cap.set(4, 720 * 0.5)  # height)

    while True:

        ret, frame = cap.read()
        if ret:

            cv2.imshow("original", frame)

            if analyzer.has_container(frame):
                #sounds.blip()
                print("FOUND!")

            #cv2.waitKey()

            if cv2.waitKey(1) & 0xFF == ord('q'):
                break

# When everything done, release the capture
cv2.destroyAllWindows()