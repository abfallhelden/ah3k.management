import cv2

from ah3k_management.container_recognition.ContainerFrameAnalyzer import ContainerFrameAnalyzer
from ah3k_management.shared.ContainerColor import ContainerColor

analyzer = ContainerFrameAnalyzer(ContainerColor.Green, True)



cap = cv2.VideoCapture("/Users/michi/Desktop/container-videos/26/04.mov")

#cap.set(3, 1280 * 0.5)  # width)
#cap.set(4, 720 * 0.5)  # height)

print("Reading Video file...")
frames = []
success,image = cap.read()
while success:
    success, frame = cap.read()
    frame = cv2.flip(frame, -1)
    frames.append(frame)
cap.release()

index = 0
while True:
    try:
        frame = frames[index]
        #cv2.imshow("original", frame)
        found = analyzer.has_container(frame)
        print("frame: {}  found:{}".format(index, found))
    except:
        None


    key = cv2.waitKey()

    #print(key)
    if key == 63234:
        index -= 1
    elif key == 63235:
        index += 1
    elif key == 63233:
        index -= 10
    elif key == 63232:
        index += 10
    elif key == 45:
        index -= 100
    elif key == 43:
        index += 100
    elif key == ord('q'):
        break

# When everything done, release the capture
cv2.destroyAllWindows()