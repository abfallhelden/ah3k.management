import cv2

from ah3k_management.line_detection.RegionEndline import RegionEndline
from ah3k_management.line_detection.LineTracker import LineTracker
from ah3k_management.line_detection.RegionContour import RegionContour
from ah3k_management.line_detection.interface.LineDetection import LineDetectionDelegate
from ah3k_management.line_detection.interface.LineInfo import LineInfo
from ah3k_management.operator.Calculator import Calculator


class TheDelegate(LineDetectionDelegate):
    def __init__(self):
        self.calculator = Calculator("/home/pi/ah3k.config")

    def line_info(self, line_info: LineInfo):
        self.calculator.steer_radius_for_line_info(line_info)

show = True
camera = "Anfahrt.mov"

size_factor = 0.5

height = 720 * size_factor  #360
width = 1280 * size_factor  #640
roi_width_factor = 0.1
roiWidth = width * roi_width_factor

tresh = 198
roiList = [RegionContour(10, 0, int(height), int(roiWidth * 9), int(roiWidth * 10), tresh, 10, height, True),
           RegionContour(9, 0, int(height), int(roiWidth * 8), int(roiWidth * 9), tresh, 10, height, True),
            RegionContour(8, 0, int(height), int(roiWidth * 7), int(roiWidth * 8), tresh, 10, int(height), True)]

roiClist = RegionEndline(1, int(6*height / 10), int(height), int(roiWidth * 5), int(roiWidth * 10), tresh, True)
tracker = LineTracker(size_factor, camera, show)
tracker.set_rois(roiList)
tracker.set_crossroad_endline_roi(roiClist)
tracker.set_delegate(TheDelegate())
tracker.run()
