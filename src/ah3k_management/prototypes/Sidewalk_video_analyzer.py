import cv2

from ah3k_management.container_recognition.ContainerFrameAnalyzer import ContainerFrameAnalyzer
from ah3k_management.container_recognition.SidewalkFrameAnalyzer import SidewalkFrameAnalyzer
from ah3k_management.shared.ContainerColor import ContainerColor

analyzer = SidewalkFrameAnalyzer(True)

cap = cv2.VideoCapture("/Users/michi/Desktop/container-videos/2016-06-02/06.mov")

print("Reading Video file...")
frames = []
success, image = cap.read()
while success:
    success, frame = cap.read()
    frame = cv2.flip(frame, -1)
    frames.append(frame)
cap.release()

index = 1
while True:
    try:
        frame = frames[index]
        cv2.imshow("original", frame)
        found = analyzer.is_crossroad(frame)
        print("frame: {}  found:{}".format(index, found))
    except Exception as e:
        print("error: {}".format(e))

    key = cv2.waitKey()

    if key == 63234:
        index -= 1
    elif key == 63235:
        index += 1
    elif key == 63233:
        index -= 10
    elif key == 63232:
        index += 10
    elif key == 45:
        index -= 100
    elif key == 43:
        index += 100
    elif key == ord('q'):
        break

# When everything done, release the capture
cv2.destroyAllWindows()
