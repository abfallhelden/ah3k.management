import subprocess

import cv2

def get_cam_with_id(id):
    video0_id = str(subprocess.check_output("udevadm info /dev/video0 | grep \"E: ID_SERIAL_SHORT\"", shell=True))
    video1_id = str(subprocess.check_output("udevadm info /dev/video1 | grep \"E: ID_SERIAL_SHORT\"", shell=True))

    if id in video0_id:
        return cv2.VideoCapture(0)
    elif id in video1_id:
        return cv2.VideoCapture(1)
    else:
        return None


cam_L_id = "2014112219099"
cam_R_id = "2015062709225"

left_cam = get_cam_with_id(cam_L_id)

right_cam = get_cam_with_id(cam_R_id)

ret_l, img_left = left_cam.read()
ret_r, img_right = right_cam.read()


cv2.imwrite("/home/pi/v_left.png", img_left)
cv2.imwrite("/home/pi/v_right.png", img_right)
