import time
from picamera.array import PiRGBArray
from picamera import PiCamera
import numpy as np
import cv2

def nothing(x):
    pass

tresh = 100

# initialize the camera and grab a reference to the raw camera capture
camera = PiCamera()
camera.resolution = (640, 480)
camera.framerate = 32
rawCapture = PiRGBArray(camera, size=(640, 480))

# allow the camera to warmup
time.sleep(0.1)

#cap = cv2.VideoCapture("container_green2.mov")
#ret = cap.set(3,1280) #Set a width of 320 pixels
#ret = cap.set(4,720) #Set a height of 240 pixels

"""
% Define thresholds for channel 1 based on histogram settings
channel1Min = 0.216;
channel1Max = 0.413;

% Define thresholds for channel 2 based on histogram settings
channel2Min = 0.244;
channel2Max = 0.636;

% Define thresholds for channel 3 based on histogram settings
channel3Min = 0.325;
channel3Max = 0.605;"""


iLowH = int(255 * 0.216)
iHighH = int(255 * 0.413)

iLowS = int(255 * 0.244)
iHighS = int(255 * 0.636)

iLowV = int(255 * 0.325)
iHighV = int(255 * 0.605)

iLastX = -1
iLastY = -1

cv2.namedWindow("HSV")
cv2.createTrackbar("HUE Low", "HSV", iLowH,255,nothing)
cv2.createTrackbar("HUE High", "HSV", iHighH,255,nothing)
cv2.createTrackbar("Saturation", "HSV", iLowS,255,nothing)
cv2.createTrackbar("Value", "HSV", iLowV,255,nothing)

#ret, imgTmp = cap.read()
#camera.capture(rawCapture, format="bgr")
#imgTmp = rawCapture.array
#height, width, channels = imgTmp.shape
#imgLine = np.zeros((height, width, 3), np.uint8)

for the_frame in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True):
    # Capture frame-by-frame
    #ret, frame = cap.read()

    frame = the_frame.array

    #roi = frame[80:120,0:320];
    #roi2 = frame[100:140,0:320];
    #roi3 = frame[120:160,0:320];

    imgHSV = cv2.cvtColor(frame,cv2.COLOR_BGR2HSV)

    arrMin = np.array([iLowH, iLowS, iLowV], np.uint8)
    arrMax = np.array([iHighH, iHighS, iHighV], np.uint8)

    imgThresholded = cv2.inRange(imgHSV, arrMin, arrMax)

    erodeElmt = cv2.getStructuringElement(cv2.MORPH_RECT, (2, 2))
    dilateElmt = cv2.getStructuringElement(cv2.MORPH_RECT, (10, 10))
    imgThresholded = cv2.erode(imgThresholded, erodeElmt)
    #imgThresholded = cv2.dilate(imgThresholded, dilateElmt)

    moments = cv2.moments(imgThresholded)
    dM01 = moments['m01']
    dM10 = moments['m10']
    dArea = moments['m00']

    if dArea > 10000:
        posX = int(dM10/dArea)
        posY = int(dM01/dArea)

        cv2.circle(frame,(posX,posY), 4, (255,0,0), -1)
        cv2.circle(frame,(posX,posY), 8, (0,255,0), 0)

        #print(str("dArea: " + str(dArea)))
        #print(str("m10: " + str(dM10)))
        #print(str("m10: " + str(dM10)))
        #if(posX < 200):
            #print(str("X Pos: " + str(posX) + "\n"))
            #print("Container detected, slow down")

        #if(posX > 200):
            #print("Position reached, stop now")

        #if iLastX >= 0 and iLastY >= 0 and posX >= 0 and posY >=0:
            #cv2.line(imgLine, (posX, posY), (iLastX, iLastY), (0, 0, 255), 2);

        iLastX = posX
        iLastY = posY

    #tight = cv2.Canny(imgThresholded,225,250)
    #cv2.imshow("Canny", tight)

    cv2.imshow("Thresholded Image", imgThresholded)

    #frame = frame + imgLine
    cv2.imshow("Original", frame)

    iLowH = cv2.getTrackbarPos("HUE Low", "HSV")
    iHighH = cv2.getTrackbarPos("HUE High", "HSV")

    iLowS = cv2.getTrackbarPos("Saturation", "HSV")

    iLowV = cv2.getTrackbarPos("Value", "HSV")
    rawCapture.truncate(0)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cv2.destroyAllWindows()