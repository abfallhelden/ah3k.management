import numpy as np
import cv2

def nothing(x):
    pass

tresh = 100
blockSize = 2;
apertureSize = 3;
k = 0.04;

cap = cv2.VideoCapture(0);
ret = cap.set(3,320); #Set a width of 320 pixels
ret = cap.set(4,240); #Set a height of 240 pixels

cv2.namedWindow("Source Image")
cv2.namedWindow("Final Image")
cv2.createTrackbar("Threshold:", "Source Image", tresh,255,nothing)

while(True):
    # Capture frame-by-frame
    ret, frame = cap.read();

    height, width, channels = frame.shape
    dst = np.zeros((height, width, 3), np.uint8);
    dst_norm = np.zeros((height, width, 3), np.uint8);
    dst_norm_scaled = np.zeros((height, width, 3), np.uint8);

    cv2.imshow("Source Image", frame);

    imgGray = cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY);

    cv2.cornerHarris(imgGray, blockSize, apertureSize, k, dst, cv2.BORDER_DEFAULT)

    cv2.normalize(dst, dst_norm,0,255,cv2.NORM_MINMAX,cv2.CV_32FC1)
    cv2.convertScaleAbs(dst_norm,dst_norm_scaled)

    test = dst_norm.shape;
    print dst_norm[1,1]

    for j in range(0, test[0]):
        for i in range(0, test[1]):
            if dst_norm[j,i] > tresh:
                cv2.circle(dst_norm_scaled,(i,j), 5, (0),2,8,0)

    cv2.imshow("Final Image", dst_norm_scaled)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cv2.destroyAllWindows()