import numpy as np
import cv2

tresh = 100
cap = cv2.VideoCapture(0);
ret = cap.set(3,320); #Set a width of 320 pixels
ret = cap.set(4,240); #Set a height of 240 pixels

while(True):
    # Capture frame-by-frame
    ret, frame = cap.read();

    #roi = frame[80:120,0:320];
    #roi2 = frame[100:140,0:320];
    #roi3 = frame[120:160,0:320];

    gray = cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY);

    blurred = cv2.GaussianBlur(gray,(3,3),0)

    wide = cv2.Canny(blurred,10,200)
    tight = cv2.Canny(blurred,225,250)
    #auto = auto_canny(blurred)

    cv2.imshow("Original", frame)
    cv2.imshow("Edges", np.hstack([wide, tight]))

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cv2.destroyAllWindows()