import numpy as np
import cv2

width = 1280
height = 720
mmProPx = 0.09861111111111111
heightStart = 280
heightEnd = 400

def nothing(x):
    pass

tresh = 220
#cap = cv2.VideoCapture(1)
cap = cv2.VideoCapture("Testfahrt.mov")
ret = cap.set(3,width) #Set a width of 320 pixels
ret = cap.set(4,height) #Set a height of 240 pixels
val1 = 11
val2 = 7
cv2.namedWindow("Thresholding")
cv2.createTrackbar("Threshold", "Thresholding", tresh,255,nothing)
cv2.createTrackbar("val1", "Thresholding", val1,255,nothing)
cv2.createTrackbar("val2", "Thresholding", val2,255,nothing)

while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()

    #roi = frame[80:120,0:320];
    #roi2 = frame[100:140,0:320];
    #roi3 = frame[120:160,0:320];

    #set ROI's [height, width]
    """
    roi = frame[heightStart:500,0:(105/320*width)]
    roi2 = frame[heightStart:500,(105/320*width):(210/320*width)]
    roi3 = frame[heightStart:500,(210/320*width):width]
    """
    roi = frame[0:height,0:(105/320*width)]
    roi2 = frame[0:height,(105/320*width):(210/320*width)]
    roi3 = frame[0:height,(210/320*width):width]

    #RBG TO GRAY
    roiImg = cv2.cvtColor(roi,cv2.COLOR_BGR2GRAY)
    roiImg2 = cv2.cvtColor(roi2,cv2.COLOR_BGR2GRAY)
    roiImg3 = cv2.cvtColor(roi3,cv2.COLOR_BGR2GRAY)

    #cv2.imshow('frame1 normal',roiImg)
    #cv2.imshow('frame2 normal',roiImg2)
    #cv2.imshow('frame3 normal',roiImg3)

    cv2.imshow('gray',roiImg)

    """
    roiImg = cv2.GaussianBlur(roiImg, (10, 10), 0)
    roiImg2 = cv2.GaussianBlur(roiImg2, (5, 5), 0)
    roiImg3 = cv2.GaussianBlur(roiImg3, (5, 5), 0)
    """

    #roiImg = cv2.normalize(roiImg,1, 0, cv2.NORM_MINMAX, cv2.CV_32F)
    cv2.equalizeHist(roiImg, roiImg)

    cv2.imshow('blur',roiImg)

    #Threshold the image
    ret, roiImg = cv2.threshold(roiImg, tresh , 255, 0)
    #ret, roiImg = cvAdaptiveThreshold( temp, dst, 255,CV_ADAPTIVE_THRESH_MEAN_C,CV_THRESH_BINARY,13, 1 )
    #roiImg = cv2.adaptiveThreshold(roiImg,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY_INV,21, 15)
    #Bitwise not fur schwarze Line auf weissem Grund --> sonst auskommentieren
    #cv2.bitwise_not(roiImg,roiImg); # negative image
    ret, roiImg2 = cv2.threshold(roiImg2, tresh , 255, 0)
    #cv2.bitwise_not(roiImg2,roiImg2); # negative image
    ret, roiImg3 = cv2.threshold(roiImg3, 170 , 255, 0)
    #cv2.bitwise_not(roiImg3,roiImg3); # negative image

    cv2.imshow('frame1',roiImg)
    #cv2.imshow('frame2',roiImg2)
    #cv2.imshow('frame3',roiImg3)

    #Stoergerausche entfernen
    erodeElmt = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 5))
    dilateElmt = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 5))
    cv2.erode(roiImg, erodeElmt)
    cv2.dilate(roiImg, dilateElmt)
    cv2.erode(roiImg2, erodeElmt)
    cv2.dilate(roiImg2, dilateElmt)
    cv2.erode(roiImg3, erodeElmt)
    cv2.dilate(roiImg3, dilateElmt)

    #cv2.imshow('frame1 thresholded',roiImg)

    #Konturen finden
    _, contours, hierarchy = cv2.findContours(roiImg,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
    _, contours2, hierarchy = cv2.findContours(roiImg2,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
    _, contours3, hierarchy = cv2.findContours(roiImg3,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)

    #For Each Contour in ROI1
    lowestPoint = -1
    for i in contours:
        #Moments auslesen
        area = cv2.contourArea(i)
        moments = cv2.moments(i)
        #Grafische Gugus
        cv2.putText(frame,'Area1:' + str(area),(10,10), 2, .5,(255,0,0),2)

        #if area>1500:
        #Stoergeraeuschfilter
        if area>500:
            if moments['m00']!=0.0:
                if moments['m01']!=0.0:
                    #Centroid X und Y
                    cx = int(moments['m10']/moments['m00'])         # cx = M10/M00
                    cy = int(moments['m01']/moments['m00'])         # cy = M01/M00

                    if lowestPoint == -1:
                        lowestPoint = cy
                        lowestContour = i

                    if lowestPoint < cy:
                        lowestPoint = cy
                        lowestContour = i

                    #cv2.circle(frame,(cx,cy+80), 4, (255,0,0), -1)
                    #cv2.circle(frame,(cx,cy+80), 8, (0,255,0), 0)
                    #Kontour grafisch auf dem Originalbild markieren

    if lowestPoint != -1:
        area = cv2.contourArea(lowestContour)
        moments = cv2.moments(lowestContour)
        cx = int(moments['m10']/moments['m00'])         # cx = M10/M00
        cy = int(moments['m01']/moments['m00'])
        pxToBottom = int(height-cy)
        mmToBottom = float(pxToBottom*mmProPx)
        print("Milimeter zu Bildunterkante: " + str(mmToBottom))
        """
        cv2.circle(frame,(cx,cy+heightStart), 4, (255,0,0), -1)
        cv2.circle(frame,(cx,cy+heightStart), 8, (0,255,0), 0)
        """
        cv2.circle(frame,(cx,cy), 4, (255,0,0), -1)
        cv2.circle(frame,(cx,cy), 8, (0,255,0), 0)
        #w,h = width, height der detektierten Contour
        x,y,w,h = cv2.boundingRect(lowestContour)
        #cv2.rectangle(frame,(x,y+80),(x+w,y+h+80),(0,255,0),2)
        #cv2.rectangle(frame,(x,y+heightStart),(x+w,y+h+heightStart),(0,255,0),2)
        cv2.rectangle(frame,(x,y),(x+w,y+h),(0,255,0),2)

    lowestPoint = -1
    #ROI 2
    for i in contours2:
        area = cv2.contourArea(i)
        moments = cv2.moments(i)
        cv2.putText(frame,'Area3:' + str(area),(10,10), 2, .5,(255,0,0),2)

        if area>500:
            if moments['m00']!=0.0:
                if moments['m01']!=0.0:
                    #Centroid X und Y
                    cx = int(moments['m10']/moments['m00'])         # cx = M10/M00
                    cy = int(moments['m01']/moments['m00'])         # cy = M01/M00

                    if lowestPoint == -1:
                        lowestPoint = cy
                        lowestContour = i

                    if lowestPoint < cy:
                        lowestPoint = cy
                        lowestContour = i

    if lowestPoint != -1:
        area = cv2.contourArea(lowestContour)
        moments = cv2.moments(lowestContour)
        cx = int(moments['m10']/moments['m00'])         # cx = M10/M00
        cy = int(moments['m01']/moments['m00'])         # cy = M01/M00
        #cv2.circle(frame,(cx,cy+80), 4, (255,0,0), -1)
        #cv2.circle(frame,(cx,cy+80), 8, (0,255,0), 0)
        """
        cv2.circle(frame,(cx+int(105/320*width),cy+heightStart), 4, (255,0,0), -1)
        cv2.circle(frame,(cx+int(105/320*width),cy+heightStart), 8, (0,255,0), 0)
        """
        cv2.circle(frame,(cx+int(105/320*width),cy), 4, (255,0,0), -1)
        cv2.circle(frame,(cx+int(105/320*width),cy), 8, (0,255,0), 0)
        x,y,w,h = cv2.boundingRect(lowestContour)
        #cv2.rectangle(frame,(x,y+80),(x+w,y+h+80),(0,255,0),2)
        #cv2.rectangle(frame,(x+int(105/320*width),y+heightStart),(x+w+int(105/320*width),y+h+heightStart),(0,255,0),2)
        cv2.rectangle(frame,(x+int(105/320*width),y),(x+w+int(105/320*width),y+h),(0,255,0),2)

    lowestPoint = -1
    #ROI 3
    for i in contours3:
        area = cv2.contourArea(i)
        moments = cv2.moments(i)
        cv2.putText(frame,'Area3:' + str(area),(10,10), 2, .5,(255,0,0),2)

        if area>500:
            if moments['m00']!=0.0:
                if moments['m01']!=0.0:
                    #Centroid X und Y
                    cx = int(moments['m10']/moments['m00'])         # cx = M10/M00
                    cy = int(moments['m01']/moments['m00'])         # cy = M01/M00

                    if lowestPoint == -1:
                        lowestPoint = cy
                        lowestContour = i

                    if lowestPoint < cy:
                        lowestPoint = cy
                        lowestContour = i

    if lowestPoint != -1:
        area = cv2.contourArea(lowestContour)
        moments = cv2.moments(lowestContour)
        cx = int(moments['m10']/moments['m00'])         # cx = M10/M00
        cy = int(moments['m01']/moments['m00'])         # cy = M01/M00
        #cv2.circle(frame,(cx,cy+80), 4, (255,0,0), -1)
        #cv2.circle(frame,(cx,cy+80), 8, (0,255,0), 0
        """
        cv2.circle(frame,(cx+int(210/320*width),cy+heightStart), 4, (255,0,0), -1)
        cv2.circle(frame,(cx+int(210/320*width),cy+heightStart), 8, (0,255,0), 0)
        """
        cv2.circle(frame,(cx+int(210/320*width),cy), 4, (255,0,0), -1)
        cv2.circle(frame,(cx+int(210/320*width),cy), 8, (0,255,0), 0)
        x,y,w,h = cv2.boundingRect(lowestContour)
        #cv2.rectangle(frame,(x,y+80),(x+w,y+h+80),(0,255,0),2)
        #cv2.rectangle(frame,(x+int(210/320*width),y+heightStart),(x+w+int(210/320*width),y+h+heightStart),(0,255,0),2)
        cv2.rectangle(frame,(x+int(210/320*width),y),(x+w+int(210/320*width),y+h),(0,255,0),2)

    # Our operations on the frame come here
    #gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # Display the resulting frame
    cv2.imshow('original',frame)
    #cv2.imshow('frame1',roiImg)
    #cv2.imshow('frame2',roiImg2)
    #cv2.imshow('frame3',roiImg3)

    tresh = cv2.getTrackbarPos("Threshold", "Thresholding")
    val1 = cv2.getTrackbarPos("val1", "Thresholding")
    val2 = cv2.getTrackbarPos("val2", "Thresholding")

    """
    #Tastendruck q = abort
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
    """
    if cv2.waitKey(0) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cv2.destroyAllWindows()
cv2.VideoCapture(0).release()