from ah3k_management.line_detection.Region_contour import RegionContour
import cv2

width = 640
height = 380

tresh = 165

y2 = 650

roiWidth = 64
roiList = []
"""
roi = RegionContour(1, y1, y2, 0, roiWidth, tresh, 10)
roi2 = RegionContour(2, y1, y2, roiWidth, roiWidth*2, tresh, 10)
roi3 = RegionContour(3, y1, y2, roiWidth*2, roiWidth*3, tresh, 10)
roi4 = RegionContour(4, y1, y2, roiWidth*3, roiWidth*4, tresh, 10)
roi5 = RegionContour(5, y1, y2, roiWidth*4, roiWidth*5, tresh, 10)
roi6 = RegionContour(6, y1, y2, roiWidth*5, roiWidth*6, tresh, 10)
roi7 = RegionContour(7, y1, y2, roiWidth*6, roiWidth*7, tresh, 10)
"""
roi8 = RegionContour(8, 0, height, roiWidth*7, roiWidth*8, tresh, 10, height)
roi9 = RegionContour(9, 0, height, roiWidth*8, roiWidth*9, tresh, 10, height)
roi10 = RegionContour(10, 0, height, roiWidth*9, roiWidth*10, tresh, 10, height)
"""
roiList.append(roi)
roiList.append(roi2)
roiList.append(roi3)
roiList.append(roi4)
roiList.append(roi5)
roiList.append(roi6)
roiList.append(roi7)
"""
roiList.append(roi8)
roiList.append(roi9)
roiList.append(roi10)
"""
roiWidth_3 = int(width/3)
roi = RegionContour(1, 0, height, 0, roiWidth_3, tresh, 0)
roi2 = RegionContour(2, 0, height, roiWidth_3, roiWidth_3*2, tresh, 0)
roi3 = RegionContour(3, 0, height, roiWidth_3*2, roiWidth_3*3, tresh, 0)
roiList.append(roi)
roiList.append(roi2)
roiList.append(roi3)
"""

#cap = cv2.VideoCapture(1)
#cap = cv2.VideoCapture(0)
cap = cv2.VideoCapture("Testfahrt.mov")
ret = cap.set(3, width)
ret = cap.set(4, height)

while True:
    ret, frame = cap.read()
    frame = cv2.resize(frame,None,fx = 0.5, fy = 0.5, interpolation = cv2.INTER_CUBIC)
    cv2.imshow('frame',frame)
    if not ret:
        print("No frame")
    else:
        """
        frame = cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)
        cv2.equalizeHist(frame, frame)
        cv2.imshow("hist", frame)
        """
        for roi in roiList:
            #print("For each rois")
            roi.set_frame(frame)
            roi.convert_rgb_gray()
            roi.threshold_bitwise()
            cv2.imshow("tresh", frame)
            roi.erode_dilate()
            roi.find_contours()
            roi.search_contours()
            roi.return_line_info

        cv2.imshow("frame", frame)


        """
        WICHTIG
        Aufbau mit 10 Regionen ist am besten
        jeder Teil eines Liniensegmentes einer ROI hat eine bestimmte Höhe und Breite
        Hat man eine bestimmte Höhe überschritten, kann erkannt werden, dass es sich um eine
        quer-laufende Linie handelt

        """
    """
    #Tastendruck q = abort
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
    """
    if cv2.waitKey(0) & 0xFF == ord('q'):
        break


cv2.destroyAllWindows()
cv2.VideoCapture(0).release()