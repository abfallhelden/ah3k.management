import numpy as np
import cv2

tresh = 100
cap = cv2.VideoCapture(0);
ret = cap.set(3,320); #Set a width of 320 pixels
ret = cap.set(4,240); #Set a height of 240 pixels

while(True):
    # Capture frame-by-frame
    ret, frame = cap.read();

    #roi = frame[80:120,0:320];
    #roi2 = frame[100:140,0:320];
    #roi3 = frame[120:160,0:320];

    roi = frame[0:240,0:105];
    roi2 = frame[0:240,105:210];
    roi3 = frame[0:240,210:320];

    roiImg = cv2.cvtColor(roi,cv2.COLOR_BGR2GRAY);
    roiImg2 = cv2.cvtColor(roi2,cv2.COLOR_BGR2GRAY);
    roiImg3 = cv2.cvtColor(roi3,cv2.COLOR_BGR2GRAY);

    #cv2.imshow('frame1 normal',roiImg)
    #cv2.imshow('frame2 normal',roiImg2)
    #cv2.imshow('frame3 normal',roiImg3)

    ret, roiImg = cv2.threshold(roiImg, tresh , 255, 0);
    cv2.bitwise_not(roiImg,roiImg); # negative image
    ret, roiImg2 = cv2.threshold(roiImg2, tresh , 255, 0);
    cv2.bitwise_not(roiImg2,roiImg2); # negative image
    ret, roiImg3 = cv2.threshold(roiImg3, tresh , 255, 0);
    cv2.bitwise_not(roiImg3,roiImg3); # negative image

    erodeElmt = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3));
    dilateElmt = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 5));
    cv2.erode(roiImg, erodeElmt);
    cv2.dilate(roiImg, dilateElmt);
    cv2.erode(roiImg2, erodeElmt);
    cv2.dilate(roiImg2, dilateElmt);
    cv2.erode(roiImg3, erodeElmt);
    cv2.dilate(roiImg3, dilateElmt);

    #cv2.imshow('frame1 thresholded',roiImg)

    moments = cv2.moments(roiImg);
    dM01 = moments['m01'];
    dM10 = moments['m10'];
    dArea = moments['m00'];

    if dArea > 10000:
        cx = int(dM10/dArea);
        cy = int(dM01/dArea);
        cv2.circle(frame,(cx,cy), 4, (255,0,0), -1)
        cv2.circle(frame,(cx,cy), 8, (0,255,0), 0)

        str("roiImg1 X/Y: " + str(cx) + " , " + str(cy) + "\n")

        cv2.imshow('frame1',roiImg)

    moments = cv2.moments(roiImg2);
    dM01 = moments['m01'];
    dM10 = moments['m10'];
    dArea = moments['m00'];

    if dArea > 10000:
        cx = int(dM10/dArea);
        cy = int(dM01/dArea);
        cv2.circle(frame,(cx+105,cy), 4, (255,0,0), -1)
        cv2.circle(frame,(cx+105,cy), 8, (0,255,0), 0)

        print(str("roiImg2 X/Y: " + str(cx) + " , " + str(cy) + "\n"))

        cv2.imshow('frame2',roiImg2)

    moments = cv2.moments(roiImg3);
    dM01 = moments['m01'];
    dM10 = moments['m10'];
    dArea = moments['m00'];

    if dArea > 10000:
        cx = int(dM10/dArea);
        cy = int(dM01/dArea);
        cv2.circle(frame,(cx+210,cy), 4, (255,0,0), -1)
        cv2.circle(frame,(cx+210,cy), 8, (0,255,0), 0)

        str("roiImg3 X/Y: " + str(cx) + " , " + str(cy) + "\n")

        cv2.imshow('frame3',roiImg3)

    cv2.imshow('original',frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cv2.destroyAllWindows()