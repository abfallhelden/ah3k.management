import numpy as np
import math
import cv2

def nothing(x):
    pass

tresh = 100
cap = cv2.VideoCapture(0);
ret = cap.set(3,320); #Set a width of 320 pixels
ret = cap.set(4,240); #Set a height of 240 pixels

while(True):
    # Capture frame-by-frame
    ret, img = cap.read();

    cv2.imshow("Original", img);

    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY);
    edges = cv2.Canny(gray,80,120);
    cv2.imshow("Canny", edges);
    lines = cv2.HoughLinesP(edges, 1,math.pi/2,2,None,30,1);
    for line in lines[0]:
        if(len(line) > 3):
            pt1 = (line[0],line[1])
            pt2 = (line[2],line[3])
            cv2.line(img, pt1, pt2, (0,0,255), 3)

    cv2.imshow("Final", img)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cv2.destroyAllWindows()