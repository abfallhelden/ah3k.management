import numpy as np
import cv2
import ids

def nothing(x):
    pass

tresh = 100
cam = ids.Camera()
cam.color_mode = ids.ids_core.COLOR_RGB8    # Get images in RGB format
cam.exposure = 10                           # Set initial exposure to 5ms
#cam.auto_exposure = True
cam.continuous_capture = True               # Start image capture

iLowH = 38;
iHighH = 75;

iLowS = 34;
iHighS = 255;

iLowV = 26;
iHighV = 255;

iLastX = -1;
iLastY = -1;

cv2.namedWindow("HSV")
cv2.createTrackbar("HUE Low", "HSV", iLowH,255,nothing)
cv2.createTrackbar("HUE High", "HSV", iHighH,255,nothing)
cv2.createTrackbar("Saturation", "HSV", iLowS,255,nothing)
cv2.createTrackbar("Value", "HSV", iLowV,255,nothing)

imgTmp, ret = cam.next()
imgTmp = cv2.resize(imgTmp, (320, 240))
height, width, channels = imgTmp.shape
imgLine = np.zeros((height, width, 3), np.uint8);

while(True):
    # Capture frame-by-frame
    frame, meta = cam.next()
    frame = cv2.resize(frame, (320, 240))

    #roi = frame[80:120,0:320];
    #roi2 = frame[100:140,0:320];
    #roi3 = frame[120:160,0:320];

    imgHSV = cv2.cvtColor(frame,cv2.COLOR_BGR2HSV);

    arrMin = np.array([iLowH, iLowS, iLowV], np.uint8)
    arrMax = np.array([iHighH, iHighS, iHighV], np.uint8)

    imgThresholded = cv2.inRange(imgHSV, arrMin, arrMax)

    erodeElmt = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 5));
    dilateElmt = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 5));
    cv2.erode(imgThresholded, erodeElmt);
    cv2.dilate(imgThresholded, dilateElmt);

    moments = cv2.moments(imgThresholded);
    dM01 = moments['m01'];
    dM10 = moments['m10'];
    dArea = moments['m00'];

    if dArea > 10000:
        posX = int(dM10/dArea);
        posY = int(dM01/dArea);

        cv2.circle(frame,(posX,posY), 4, (255,0,0), -1)
        cv2.circle(frame,(posX,posY), 8, (0,255,0), 0)

        if iLastX >= 0 and iLastY >= 0 and posX >= 0 and posY >= 0:
            cv2.line(imgLine, (posX, posY), (iLastX, iLastY), (0, 0, 255), 2);

        iLastX = posX;
        iLastY = posY;

    cv2.imshow("Thresholded Image", imgThresholded);

    frame = frame + imgLine;
    cv2.imshow("Original", frame);

    iLowH = cv2.getTrackbarPos("HUE Low", "HSV");
    iHighH = cv2.getTrackbarPos("HUE High", "HSV");

    iLowS = cv2.getTrackbarPos("Saturation", "HSV");

    iLowV = cv2.getTrackbarPos("Value", "HSV");

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cv2.destroyAllWindows()