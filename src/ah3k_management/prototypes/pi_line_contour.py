import numpy as np
import cv2
import ids

tresh = 100
cam = ids.Camera()
cam.color_mode = ids.ids_core.COLOR_RGB8    # Get images in RGB format
cam.exposure = 10                            # Set initial exposure to 5ms
cam.auto_exposure = True
cam.continuous_capture = True               # Start image capture
#cap = cv2.VideoCapture(0)

while(True):
    # Capture frame-by-frame
    #ret, frame = cap.read()
    frame, meta = cam.next()
    frame = cv2.resize(frame, (320, 240))

    roi = frame[0:240,0:105];
    roi2 = frame[0:240,105:210];
    roi3 = frame[0:240,210:320];

    roiImg = cv2.cvtColor(roi,cv2.COLOR_BGR2GRAY);
    roiImg2 = cv2.cvtColor(roi2,cv2.COLOR_BGR2GRAY);
    roiImg3 = cv2.cvtColor(roi3,cv2.COLOR_BGR2GRAY);

    #cv2.imshow('frame1 normal',roiImg)
    #cv2.imshow('frame2 normal',roiImg2)
    #cv2.imshow('frame3 normal',roiImg3)

    ret, roiImg = cv2.threshold(roiImg, tresh , 255, 0);
    #cv2.bitwise_not(roiImg,roiImg); # negative image
    ret, roiImg2 = cv2.threshold(roiImg2, tresh , 255, 0);
    #cv2.bitwise_not(roiImg2,roiImg2); # negative image
    ret, roiImg3 = cv2.threshold(roiImg3, tresh , 255, 0);
    #cv2.bitwise_not(roiImg3,roiImg3); # negative image

    erodeElmt = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 5));
    dilateElmt = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 5));
    cv2.erode(roiImg, erodeElmt);
    cv2.dilate(roiImg, dilateElmt);
    cv2.erode(roiImg2, erodeElmt);
    cv2.dilate(roiImg2, dilateElmt);
    cv2.erode(roiImg3, erodeElmt);
    cv2.dilate(roiImg3, dilateElmt);

    #cv2.imshow('frame1 thresholded',roiImg)

    _, contours, hierarchy = cv2.findContours(roiImg,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
    _, contours2, hierarchy = cv2.findContours(roiImg2,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
    _, contours3, hierarchy = cv2.findContours(roiImg3,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)

    for i in contours:
        area = cv2.contourArea(i)
        moments = cv2.moments (i)
        #cv2.putText(frame,'Area1:' + str(area),(10,10), 2, .5,(255,0,0),2)

        #if area>1500:
        if area>1000:
            if moments['m00']!=0.0:
                if moments['m01']!=0.0:
                    cx = int(moments['m10']/moments['m00'])         # cx = M10/M00
                    cy = int(moments['m01']/moments['m00'])         # cy = M01/M00
                    #cv2.circle(frame,(cx,cy+80), 4, (255,0,0), -1)
                    #cv2.circle(frame,(cx,cy+80), 8, (0,255,0), 0)
                    cv2.circle(frame,(cx,cy), 4, (255,0,0), -1)
                    cv2.circle(frame,(cx,cy), 8, (0,255,0), 0)
                    x,y,w,h = cv2.boundingRect(i)
                    #cv2.rectangle(frame,(x,y+80),(x+w,y+h+80),(0,255,0),2)
                    cv2.rectangle(frame,(x,y),(x+w,y+h),(0,255,0),2)

    for i in contours2:
        area = cv2.contourArea(i)
        moments = cv2.moments (i)
        #cv2.putText(frame,'Area3:' + str(area),(10,10), 2, .5,(255,0,0),2)

        if area>1000:
            if moments['m00']!=0.0:
                if moments['m01']!=0.0:
                    cx = int(moments['m10']/moments['m00'])         # cx = M10/M00
                    cy = int(moments['m01']/moments['m00'])         # cy = M01/M00
                    #cv2.circle(frame,(cx,cy+80), 4, (255,0,0), -1)
                    #cv2.circle(frame,(cx,cy+80), 8, (0,255,0), 0)
                    cv2.circle(frame,(cx+105,cy), 4, (255,0,0), -1)
                    cv2.circle(frame,(cx+105,cy), 8, (0,255,0), 0)
                    x,y,w,h = cv2.boundingRect(i)
                    #cv2.rectangle(frame,(x,y+80),(x+w,y+h+80),(0,255,0),2)
                    cv2.rectangle(frame,(x+105,y),(x+w+105,y+h),(0,255,0),2)

    for i in contours3:
        area = cv2.contourArea(i)
        moments = cv2.moments (i)
        #cv2.putText(frame,'Area3:' + str(area),(10,10), 2, .5,(255,0,0),2)

        if area>1000:
            if moments['m00']!=0.0:
                if moments['m01']!=0.0:
                    cx = int(moments['m10']/moments['m00'])         # cx = M10/M00
                    cy = int(moments['m01']/moments['m00'])         # cy = M01/M00
                    #cv2.circle(frame,(cx,cy+80), 4, (255,0,0), -1)
                    #cv2.circle(frame,(cx,cy+80), 8, (0,255,0), 0
                    cv2.circle(frame,(cx+210,cy), 4, (255,0,0), -1)
                    cv2.circle(frame,(cx+210,cy), 8, (0,255,0), 0)
                    x,y,w,h = cv2.boundingRect(i)
                    #cv2.rectangle(frame,(x,y+80),(x+w,y+h+80),(0,255,0),2)
                    cv2.rectangle(frame,(x+210,y),(x+w+210,y+h),(0,255,0),2)

    # Our operations on the frame come here
    #gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # Display the resulting frame
    cv2.imshow('original',frame);
    cv2.imshow('frame1',roiImg)
    cv2.imshow('frame2',roiImg2)
    cv2.imshow('frame3',roiImg3)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cv2.destroyAllWindows()