import cv2

cap = cv2.VideoCapture(1)
ret = cap.set(3,1280)
ret = cap.set(4,720)

while True:
    ret, frame = cap.read()

    cv2.imshow("frame", frame)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cv2.destroyAllWindows()
cv2.VideoCapture(0).release()