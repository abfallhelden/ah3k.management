import numpy as np
import cv2
import ids

camx = ids.Camera()
camx.color_mode = ids.ids_core.COLOR_RGB8    # Get images in RGB format
camx.exposure = 5                            # Set initial exposure to 5ms
camx.auto_exposure = True
camx.continuous_capture = True
cam = ids.Camera()
cam.color_mode = ids.ids_core.COLOR_RGB8    # Get images in RGB format
cam.exposure = 5                            # Set initial exposure to 5ms
cam.auto_exposure = True
cam.continuous_capture = True               # Start image capture
#cap = cv2.VideoCapture(0)

while(True):
    # Capture frame-by-frame
    #ret, frame = cap.read()
    img, meta = cam.next()
    img2, meta = camx.next()

    # Our operations on the frame come here
    #gray1 = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    #gray2 = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)

    # Display the resulting frame
    cv2.imshow('Kamera 1',img)
    cv2.imshow('Kamera 2',img2)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cv2.destroyAllWindows()
