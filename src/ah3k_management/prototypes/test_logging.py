import logging
import test_logging2
logging.basicConfig(filename='logger_abc.log',level=logging.DEBUG)
logging.debug('This message should go to the log file')
logging.info('So should this')
logging.warning('And this, too')

test_logging2.doLog()

"""
Tutorial
https://docs.python.org/3/howto/logging.html

http://stackoverflow.com/questions/13479295/python-using-basicconfig-method-to-log-to-console-and-file

"""