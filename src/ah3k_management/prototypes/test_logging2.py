import logging

def doLog():
    logging.debug('This message should go to the log file')
    logging.info('So should this')
    logging.warning('And this, too')
    print("Logging 2 executed")