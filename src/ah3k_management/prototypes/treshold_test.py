import numpy as np
import cv2

def nothing(x):
    pass

tresh = 200
height = 1280
width = 720
cap = cv2.VideoCapture(0)
ret = cap.set(3,width)
ret = cap.set(4,height)

cv2.namedWindow("Thresholding")
cv2.createTrackbar("Threshold", "Thresholding", tresh,255,nothing)

while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()

    frame = cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)

    ret, frame = cv2.threshold(frame, tresh , 255, 0)

    #tresh = cv2.getTrackbarPos("Threshold", "Thresholding")
    tresh = cv2.getTrackbarPos("Threshold", "Thresholding")

    cv2.imshow("treshold", frame)

    #Tastendruck q = abort
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cv2.destroyAllWindows()
cv2.VideoCapture(0).release()