import numpy as np
import cv2

def nothing(x):
    pass

tresh = 200
height = 1280
width = 720
cap = cv2.VideoCapture(0)
ret = cap.set(3,height)
ret = cap.set(4,width)

cv2.namedWindow("Thresholding")
cv2.createTrackbar("Threshold", "Thresholding", tresh,255,nothing)

while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()

    frame_new = frame

    frame_new = cv2.cvtColor(frame_new,cv2.COLOR_BGR2GRAY)

    ret, frame_new = cv2.threshold(frame_new, tresh , 255, 0)
    #cv2.bitwise_not(frame_new,frame_new)

    totalPixels = height * width
    #moments = cv2.moments(frame_new)
    #dM01 = moments['m00']
    #print("Amount of white Pixels " + str(dM01))

    blackPixel = cv2.countNonZero(frame_new)
    print("Black Pixel: " + str(blackPixel))

    if (totalPixels-blackPixel) < 90000:
        tresh += 1

    elif (totalPixels-blackPixel) > 92000:
        tresh -= 1
    else:
        break

    # Display the resulting frame
    cv2.imshow('original',frame)
    cv2.imshow('tresholded',frame_new)

    #tresh = cv2.getTrackbarPos("Threshold", "Thresholding")
    cv2.setTrackbarPos("Threshold", "Thresholding", tresh)

    #Tastendruck q = abort
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

print("Final Treshold:" + str(tresh))
# When everything done, release the capture
cv2.destroyAllWindows()
cv2.VideoCapture(0).release()