import logging
import os
from configparser import ConfigParser


class Configuration:
    def __init__(self, config_path: str):
        self.config_path = config_path

        if not self.read():
            self.__set_default_config()

    def read(self) -> bool:
        return self.__read(self.config_path)

    def __read(self, file) -> bool:
        """
        Reads config from file or uses default settings if file does not exist.
        :param file:
        :return:
        """
        if os.path.exists(file):
            try:
                config = ConfigParser()
                config.read(file)

                self.line_detection_cam_id = config['system']['line_detection_cam_id']
                self.container_recognition_cam_id = config['system']['container_recognition_cam_id']
                self.default_speed = config['driving']['default_speed']
                self.optimal_distance = config['driving']['optimal_distance']
                self.k_constant_left = config['driving']['k_constant_left']
                self.k_constant_right = config['driving']['k_constant_right']
                self.line_camera_treshold = config['driving']['line_camera_treshold']
                self.line_camera_exposure_time = config['driving']['line_camera_exposure_time']
                self.log_path = config['logging']['path']
                self.log_level = int(config['logging']['level'])

                return True
            except:
                return False

        return False

    def save(self):
        config = ConfigParser()

        config.add_section('system')
        config.set('system', 'line_detection_cam_id', str(self.line_detection_cam_id))
        config.set('system', 'container_recognition_cam_id', str(self.container_recognition_cam_id))

        config.add_section('driving')
        config.set('driving', 'default_speed', str(self.default_speed))
        config.set('driving', 'optimal_distance', str(self.optimal_distance))
        config.set('driving', 'k_constant_left', str(self.k_constant_left))
        config.set('driving', 'k_constant_right', str(self.k_constant_right))
        config.set('driving', 'line_camera_treshold', str(self.line_camera_treshold))
        config.set('driving', 'line_camera_exposure_time', str(self.line_camera_exposure_time))

        config.add_section('logging')
        config.set('logging', 'path', self.log_path)
        config.set('logging', 'level', str(self.log_level))

        with open(self.config_path, 'w') as configfile:
            config.write(configfile)

    def __set_default_config(self):
        self.line_detection_cam_id = "2015062709225"
        self.container_recognition_cam_id = "2014112219099"

        self.default_speed = 180
        self.optimal_distance = 42
        self.k_constant_left = 6700
        self.k_constant_right = 6000
        self.line_camera_treshold = 130
        self.line_camera_exposure_time = 45

        self.log_path = 'ah3k.log'
        self.log_level = logging.DEBUG

    @staticmethod
    def str2bool(v):
        return v.lower() in ("yes", "true", "t", "1")
