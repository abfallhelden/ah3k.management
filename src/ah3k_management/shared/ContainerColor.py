from enum import Enum

import numpy as np


class ContainerColor(Enum):
    Undefined = -1
    Blue = 0
    Green = 1

    @property
    def ycrcb_range(self):

        if self.value == 0:  # Blue
            return np.array([47, 153, 20], np.uint8), np.array([186, 204, 105], np.uint8)
        elif self.value == 1:  # Green
            return np.array([74, 77, 72], np.uint8), np.array([124, 117, 122], np.uint8)


"""channel1Min = 47.000;
channel1Max = 156.000;

% Define thresholds for channel 2 based on histogram settings
channel2Min = 163.000;
channel2Max = 204.000;

% Define thresholds for channel 3 based on histogram settings
channel3Min = 64.000;
channel3Max = 105.000;"""
