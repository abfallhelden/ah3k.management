from enum import Enum


class DrivingStatus(Enum):
    Standing = 0
    Accelerating = 1
    Moving = 2
    Stopping = 3
    Any = 7
