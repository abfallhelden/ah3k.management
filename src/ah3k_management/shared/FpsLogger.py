import logging
from datetime import datetime, timedelta


class FpsLogger:
    def __init__(self, name: str):
        self.name = name
        self.fps_count = 0
        self.fps_time = datetime.utcnow()
        self.fps_list = []

    def tick(self):
        self.fps_count += 1
        now = datetime.utcnow()
        if (now - self.fps_time) >= timedelta(seconds=1):
            logging.debug("{}: {} FPS".format(self.name, self.fps_count))

            #self.fps_list.append(self.fps_count)

            self.fps_time = now

            self.fps_count = 0

    def log(self):
        logging.info("fps: {}".format(self.fps_list))
