from enum import Enum


class Status(Enum):
    Start = 0
    """ The system just started, power is up, no initialization done."""

    Initializing = 1
    """ The system is initializing its components. """

    Ready = 2
    """ The system is ready for the procedure to be started. """

    Driving = 3
    """ The vehicle is driving. """

    ReachingContainer = 4
    """ A Container has been recognized, vehicle now reaching the container. """

    EmptyingContainer = 5
    """ Grabbing container and emptying it. """

    WaitingForPrecedence = 6
    """ Waiting for another car to give the road free. """

    ReachingEndLine = 7
    """ The end area was recognized, reaching it now. """

    EmptyingLoad = 8
    """ Emptying the load in the end waste container. """

    End = 9
    """ Procedure ended. """

    Error = 10
    """ An Error occurred. """

    ControllerReady = 11
    """ Controller is ready but management not."""

    ManagementReady = 12
    """ Management is ready but Controller not."""

    Any = 15
    """ This Status is used for Rules that should apply in any status."""
