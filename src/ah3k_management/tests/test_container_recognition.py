import os
from unittest import TestCase

import cv2

from ah3k_management.container_recognition.ContainerFrameAnalyzer import ContainerFrameAnalyzer
from ah3k_management.shared.ContainerColor import ContainerColor


class TestContainerRecognition(TestCase):
    def setUp(self):
        self.analyzer = ContainerFrameAnalyzer(ContainerColor.Green, False)

    def run_analyzation(self, expected, folder, message):
        images = self._load_images_from_folder(folder)
        wrong_detections = []
        for (filename, frame) in images:
            result = self.analyzer.has_container(frame)
            if result != expected:
                wrong_detections.append(filename)
        self.assertEqual(0, len(wrong_detections),
                         "{}.\n{} correct, {} wrong: {}".format(
                             message, len(images) - len(wrong_detections), len(wrong_detections), wrong_detections))

    def test_green_positive(self):
        self.analyzer.color = ContainerColor.Green
        folder = "resources/green/"
        message = "Expected to find green container in images"
        expected = True

        self.run_analyzation(expected, folder, message)

    def test_blue_positive(self):
        self.analyzer.color = ContainerColor.Blue
        folder = "resources/blue/"
        message = "Expected to find blue container in images"
        expected = True

        self.run_analyzation(expected, folder, message)

    def test_green_negative_for_no_container(self):
        self.analyzer.color = ContainerColor.Green
        folder = "resources/no/"
        message = "Expected to find NO green container in images"
        expected = False

        self.run_analyzation(expected, folder, message)


    def test_blue_negative_for_no_container(self):
        self.analyzer.color = ContainerColor.Blue
        folder = "resources/no/"
        message = "Expected to find NO blue container in images"
        expected = False

        self.run_analyzation(expected, folder, message)

    def test_green_negative_for_blue(self):
        self.analyzer.color = ContainerColor.Green
        folder = "resources/blue/"
        message = "Expected to find NO green container in images"
        expected = False

        self.run_analyzation(expected, folder, message)


    def test_blue_negative_for_green(self):
        self.analyzer.color = ContainerColor.Blue
        folder = "resources/green/"
        message = "Expected to find NO blue container in images"
        expected = False

        self.run_analyzation(expected, folder, message)


    def _load_images_from_folder(self, folder):
        images = []
        for filename in os.listdir(folder):
            img = cv2.imread(os.path.join(folder,filename))
            if img is not None:
                images.append((filename, img))
        return images