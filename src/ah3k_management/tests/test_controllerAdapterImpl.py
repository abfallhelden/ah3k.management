import time
from threading import Condition
from unittest import TestCase

from ah3k_management.controller_adapter.Communicator import Communicator, CommandLineCommunicator
from ah3k_management.controller_adapter.ControllerAdapterImpl import ControllerAdapterImpl
from ah3k_management.controller_adapter.interface.ControllerAdapter import ControllerAdapterDelegate
from ah3k_management.shared.Status import Status
from ah3k_management.shared.TerminalColors import TerminalColors


class TestControllerAdapterImpl(TestCase):
    def setUp(self):
        self.communicator = CommunicatorMock()
        self.controller = ControllerAdapterImpl(self.communicator)
        self.operator = ControllerAdapterDelegateMock()

        self.controller.set_delegate(self.operator)
        self.controller.init()

    def tearDown(self):
        self.controller.set_status(Status.End)
        self.controller.stop_listener()

    def test_waitAtCrossroad(self):
        self.communicator.set_input("crossroadclean")
        result = self.controller.wait_for_clean_crossroad()
        self.assertTrue("crossroad" in self.communicator.lines)

    def test_setStatus(self):
        self.controller.set_status(Status.EmptyingLoad)
        self.assertTrue("setstatus 8" in self.communicator.lines)

    def test_start(self):
        self.communicator.set_input("start 0")
        time.sleep(0.01)
        self.assertEqual(self.operator.start_calls, 1)

    def test_check_interface_compatibility_successful(self):
        #self.controller.set_status(Status.End)
        self.communicator.set_input("ifversion {}".format(self.controller.version))
        compatibility = self.controller.is_interface_compatible()
        self.assertTrue(compatibility)

    def test_check_interface_compatibility_fail(self):
        self.controller.set_status(Status.End)
        self.communicator.set_input("ifversion 0.0.1")
        compatibility = self.controller.is_interface_compatible()
        self.assertFalse(compatibility)


class CommunicatorMock(CommandLineCommunicator):

    def __init__(self):
        super().__init__()
        self.lines = []
        self.input_line = ""
        self.cv = Condition()
        self.usedefaultanswers = False

    def set_input(self, input_line: str):
        super().set_answer(input_line + "\n")

    def send_line(self, line: str):
        super().send_line(line)
        self.lines.append(line)


class ControllerAdapterDelegateMock(ControllerAdapterDelegate):
    start_calls = 0
    objecttracked_calls = 0

    def start(self, container_color):
        self.start_calls += 1
