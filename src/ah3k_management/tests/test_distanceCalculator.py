import unittest

import time

from ah3k_management.operator.DistanceCalculator import DistanceCalculator


class test_distanceCalculator(unittest.TestCase):
    def test_measurement_with_no_run(self):
        dc = DistanceCalculator(1000)

        result = dc.get_distance()

        self.assertEqual(result, 0)

    def test_measurement_with_one_run(self):
        dc = DistanceCalculator(1000)

        dc.start()
        time.sleep(0.2)
        dc.stop()
        result = dc.get_distance()

        self.assertGreater(result, 190)
        self.assertLess(result, 210)

    def test_measurement_with_three_runs(self):
        dc = DistanceCalculator(1000)

        dc.start()
        time.sleep(0.1)
        dc.stop()
        time.sleep(0.1)
        dc.start()
        time.sleep(0.1)
        dc.stop()
        time.sleep(0.1)
        dc.start()
        time.sleep(0.1)
        dc.stop()
        result = dc.get_distance()

        self.assertGreater(result, 290)
        self.assertLess(result, 315)

    def test_measurement_with_unfinished_run(self):
        dc = DistanceCalculator(1000)

        dc.start()
        time.sleep(0.3)
        result = dc.get_distance()

        self.assertGreater(result, 290)
        self.assertLess(result, 315)

    def test_measurement_with_three_runs_one_unfinished(self):
        dc = DistanceCalculator(1000)

        dc.start()
        time.sleep(0.1)
        dc.stop()
        time.sleep(0.1)
        dc.start()
        time.sleep(0.1)
        dc.stop()
        time.sleep(0.1)
        dc.start()
        time.sleep(0.1)
        result = dc.get_distance()

        self.assertGreater(result, 290)
        self.assertLess(result, 315)

    def test_distance_splitting(self):
        dc = DistanceCalculator(1500)

        dc.start()
        time.sleep(1)
        dc.stop()
        result = dc.get_distance()

        distance_m = int(result / 1000)
        distance_cm = int(result / 10 - distance_m * 100)

        self.assertGreater(result, 1490)
        self.assertLess(result, 1515)

        self.assertEqual(distance_m, 1)
        self.assertGreater(distance_cm, 49)
        self.assertLess(distance_cm, 52)
