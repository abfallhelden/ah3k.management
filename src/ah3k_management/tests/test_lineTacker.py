from ah3k_management.line_detection.RegionContour import RegionContour
from ah3k_management.line_detection.interface.Region import Region
from ah3k_management.line_detection.LineTracker import LineTracker
from unittest import TestCase


class TestLineTracker(TestCase):
    def setUp(self):
        self.lineTracker = LineTrackerMock()
        self.region = RegionContourMock()
        pass

    def tearDown(self):
        pass

    def test_isAliveSet_true(self):
        self.lineTracker.set_is_alive(True)
        is_alive = self.lineTracker.get_is_alive()
        self.assertTrue(is_alive)

    def test_isAliveSet_false(self):
        self.lineTracker.set_is_alive(False)
        is_alive = self.lineTracker.get_is_alive()
        self.assertFalse(is_alive)


class RegionContourMock(Region):
    def __init__(self):
        pass


class LineTrackerMock(LineTracker):
    def __init__(self):
        pass

    def get_is_alive(self) -> bool:
        return self.isAlive
