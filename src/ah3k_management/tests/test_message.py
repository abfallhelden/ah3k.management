import logging

from ah3k_management.controller_adapter.Message import Message


import unittest



class TestMessage(unittest.TestCase):

    def test_parse(self):
        line = "speed 25 12"
        message = Message(line)
        self.assertEqual(message.command, "speed")
        self.assertEqual(message.parameters[0], "25")
        self.assertEqual(message.parameters[1], "12")

    def test_to_string(self):
        message = Message("")
        message.command = "command"

        message.parameters.append('p0')
        message.parameters.append('p1')
        line = message.to_string()

        self.assertEqual(line, "command p0 p1")

    def test_blabla(self):
        it = logging.DEBUG