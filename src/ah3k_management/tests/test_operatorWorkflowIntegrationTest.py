import threading
import time
import unittest
from multiprocessing import Queue
from unittest.mock import MagicMock

from ah3k_management.container_recognition.interface.ContainerRecognition import ContainerRecognitionDelegate
from ah3k_management.controller_adapter.ControllerAdapterImpl import ControllerAdapterImpl
from ah3k_management.controller_adapter.interface.ControllerAdapter import ControllerAdapterDelegate
from ah3k_management.line_detection.interface.LineDetection import LineDetectionDelegate
from ah3k_management.line_detection.interface.LineInfo import LineInfo
from ah3k_management.operator.InterProcessInputCollector import InterProcessInputCollector
from ah3k_management.operator.Operator import Operator
from ah3k_management.shared.Configuration import Configuration
from ah3k_management.shared.ContainerColor import ContainerColor
from ah3k_management.shared.DrivingStatus import DrivingStatus
from ah3k_management.shared.Status import Status


class OperatorWorkflowIntegrationTests(unittest.TestCase):
    __time_delay = 0.1

    def set_controller_delegate(self, delegate: ControllerAdapterDelegate):
        self.controller_adapter_delegate = delegate

    def set_container_delegate(self, delegate: ContainerRecognitionDelegate):
        self.container_recognition_delegate = delegate

    def set_line_delegate(self, delegate: LineDetectionDelegate):
        print("setting line delegate.")
        self.line_detection_delegate = delegate

    def setUp(self):
        queue = Queue()

        # Controller Adapter
        self.controller_adapter = MagicMock()
        self.controller_adapter.get_interface_version = MagicMock(return_value=ControllerAdapterImpl.version)
        self.controller_adapter.version = ControllerAdapterImpl.version

        self.controller_adapter.set_delegate = MagicMock(side_effect=self.set_controller_delegate)

        collector = InterProcessInputCollector(queue)

        # Line Detection
        self.line_detection = MagicMock()
        self.set_line_delegate(collector)

        # Container Recognition
        self.container_recognition = MagicMock()
        self.set_container_delegate(collector)

        # Configuration
        self.configuration = Configuration("")
        self.configuration.default_speed = 256

        # noinspection PyTypeChecker
        self.operator = Operator(self.controller_adapter, self.line_detection, self.container_recognition,
                                 self.configuration, input_queue=queue)

    def tearDown(self):
        self.operator.resources.status = Status.End
        self.operator.resources.driving_status = DrivingStatus.Standing
        self.controller_adapter_delegate.battery_low()

    def test_setDelegates(self):
        self.operatorthread = threading.Thread(target=self.operator.run, name="Operator_MainThread")
        self.operatorthread.start()
        time.sleep(self.__time_delay)

        self.assertIsNotNone(self.controller_adapter_delegate)

        self.assertIsNotNone(self.line_detection_delegate)

        # todo: uncomment as soon as it is implemented.
        # assert self.container_recognition.set_delegate.called
        # self.assertIsNotNone(self.container_recognition_delegate)

    def test_bootUpShutDown(self):
        # Step 1
        # Start the Operator in a new Thread.
        # It should now check the interface version and set the "init" command to the controller.
        self.operatorthread = threading.Thread(target=self.operator.run, name="Operator_MainThread")
        self.operatorthread.start()
        time.sleep(self.__time_delay)
        self.controller_adapter.get_interface_version.assert_called_with()
        self.controller_adapter.set_status.assert_called_with(Status.Initializing)
        self.controller_adapter.init.assert_called_with()
        assert not self.line_detection.start_tracking.called
        assert not self.container_recognition.start_tracking.called

        # Step 2
        # The controller sends "ready", then the status should change to Ready.
        self.controller_adapter_delegate.ready()
        time.sleep(self.__time_delay*10)
        self.controller_adapter.set_status.assert_called_with(Status.Ready)

        # Step 3
        # The Controller sends "batterylow", on which the operator should stop all threads and end the program.
        self.controller_adapter_delegate.battery_low()
        time.sleep(self.__time_delay)
        assert self.line_detection.stop_tracking.called
        #assert self.container_recognition.stop_tracking.called

    def test_testDriveFirstDrive(self):
        self.__go_to_ready()

        # Step 1
        # The user presses start. After that the Operator should:
        # - Set Status to "Driving"
        # - Start Line Tracking
        # - Start Container Tracking witch correct Color
        # - Send a "speed" command to the Controller
        color = ContainerColor.Green
        self.controller_adapter_delegate.start(color)
        time.sleep(self.__time_delay)

        self.controller_adapter.set_status.assert_called_with(Status.Driving)
        self.line_detection.start_tracking.assert_called_with()
        self.container_recognition.start_tracking.assert_called_with(color)
        assert self.controller_adapter.speed.called

    def test_startDriveAndLineTracking(self):
        self.__go_to_ready()

        # Step 1
        # The user presses start. After that the Operator should:
        # - Set Status to "Driving"
        # - Start Line Tracking
        # - Start Container Tracking witch correct Color
        # - Send a "speed" command to the Controller
        color = ContainerColor.Green
        self.controller_adapter_delegate.start(color)
        time.sleep(self.__time_delay)

        self.controller_adapter.set_status.assert_called_with(Status.Driving)
        self.line_detection.start_tracking.assert_called_with()
        self.container_recognition.start_tracking.assert_called_with(color)
        self.controller_adapter.speed.assert_called_with(self.configuration.default_speed, 0)

        # Step 2
        # Line Tracking sends a LineInfo. After that the Operator should send a steer command to the controller.
        line_info = LineInfo(6)
        self.line_detection_delegate.line_info(line_info)
        time.sleep(self.__time_delay)
        assert self.controller_adapter.steer.called

        # Step 3
        # Speed reached
        self.controller_adapter_delegate.speed_reached()
        time.sleep(self.__time_delay)
        line_info = LineInfo(6)
        self.line_detection_delegate.line_info(line_info)
        time.sleep(self.__time_delay)
        assert self.controller_adapter.steer.called

    @unittest.skip("not implemented yet")
    def test_containerRecognition(self):
        self.__go_to_ready()
        color = ContainerColor.Blue
        self.controller_adapter_delegate.start(color)
        time.sleep(self.__time_delay)
        self.controller_adapter_delegate.speed_reached()
        time.sleep(self.__time_delay)

        # Step 1
        # ContainerRecognition sends container info. After that the Operator should:
        # - Set the status to ReachingContainer
        #  - Send a "stop for container and grab".
        self.container_recognition_delegate.container_detected()
        time.sleep(self.__time_delay)
        self.controller_adapter.set_status.assert_called_with(Status.ReachingContainer)
        assert self.controller_adapter.stop_for_container_and_grab.called

        # Step 3
        # Controller sends "speed reached" message. After that the operator should:
        # - SetStatusAction(EmptyingContainer)
        # - StopContainerRecognitionAction
        # - StopLineDetectionAction
        self.controller_adapter_delegate.speed_reached()
        time.sleep(self.__time_delay)
        self.controller_adapter.set_status.assert_called_with(Status.EmptyingContainer)
        assert self.line_detection.stop_tracking.called
        assert self.container_recognition.stop_tracking.called

        # Step 4
        # Controller Adapter sends a message, that it finished emptying the container. Operator sould:
        # - Start Line Detection
        # - Start Container Recognition
        # - Set Status to Driving
        # - Send a speed command with speed.
        self.controller_adapter_delegate.container_dropped()
        time.sleep(self.__time_delay)
        self.controller_adapter.set_status.assert_called_with(Status.Driving)
        self.line_detection.start_tracking.assert_called_with()
        self.container_recognition.start_tracking.assert_called_with(color)
        assert self.controller_adapter.speed.called

    @unittest.skip("not implemented yet")
    def test_containerRecognition_error_case(self):
        self.__go_to_ready()
        color = ContainerColor.Blue
        self.controller_adapter_delegate.start(color)
        time.sleep(self.__time_delay)
        self.controller_adapter_delegate.speed_reached()
        time.sleep(self.__time_delay)

        # Step 1
        # ContainerRecognition sends container info. After that the Operator should:
        # - Set the status to ReachingContainer
        # - Send a speed command with speed [half speed] and some "effective" value.
        self.container_recognition_delegate.container_detected()
        time.sleep(self.__time_delay)
        self.controller_adapter.set_status.assert_called_with(Status.ReachingContainer)
        self.controller_adapter.speed.assert_called_with(self.configuration.default_speed / 2, 0)

        # Step 2
        # ContainerRecognition send correcting ContainerInfo. After that the Operator should:
        # - Send a "stop for container and grab".
        self.container_recognition_delegate.container_detected()
        time.sleep(self.__time_delay)
        self.controller_adapter.set_status.assert_called_with(Status.ReachingContainer)
        assert self.controller_adapter.stop_for_container_and_grab.called

        # Step 3
        # Controller sends "container not found" message. After that the operator should:
        # - SpeedAction(??)
        # - SetStatusAction(Driving)
        self.controller_adapter_delegate.container_not_found()
        time.sleep(self.__time_delay)
        self.controller_adapter.set_status.assert_called_with(Status.Driving)
        self.controller_adapter.speed.assert_called_with(self.configuration.default_speed, 0)

    @unittest.skip("not implemented yet")
    def test_crossroad(self):
        self.__go_to_ready()
        color = ContainerColor.Green
        self.controller_adapter_delegate.start(color)
        time.sleep(self.__time_delay)

        # Step 1
        # Line Tracking notifies "crossroad detected". Operator should
        # - set speed to 0.
        # - set state to "WaitingForPrecedence"
        self.container_recognition_delegate.crossroad_detected()
        time.sleep(self.__time_delay)
        self.controller_adapter.speed.assert_called_with(speed=0)
        self.controller_adapter.set_status.assert_called_with(Status.WaitingForPrecedence)

        # Step 2
        # Controller says "speed reached". Operator should call "wait for clean crossroad".
        self.controller_adapter_delegate.speed_reached()
        time.sleep(self.__time_delay)
        self.controller_adapter.wait_for_clean_crossroad.assert_called_with()

        # Step 3
        # Controller says "crossroad clean". Operator should
        # - Send speed message
        # - Set Status to "Driving".
        self.controller_adapter_delegate.crossroad_clean()
        time.sleep(self.__time_delay)
        assert self.controller_adapter.speed.called
        self.controller_adapter.set_status.assert_called_with(Status.Driving)

    @unittest.skip("not implemented yet")
    def test_endOfRide(self):
        self.__go_to_ready()
        color = ContainerColor.Green
        self.controller_adapter_delegate.start(color)
        time.sleep(self.__time_delay)

        # Step 1
        # Line Detection says "end line detected". operator should
        # - Set status to ReachingEndLine
        # - set speed to 0
        # - stop container recognition
        self.line_detection_delegate.end_line_detected()
        time.sleep(self.__time_delay)
        self.controller_adapter.set_status.assert_called_with(Status.ReachingEndLine)
        self.controller_adapter.speed.assert_called_with(speed=0)
        self.container_recognition.stop_tracking.assert_called_with()

        # Step 2
        # Controller says "speed reached". Operator should:
        # - Stop line detection
        # - set status to EmptyingLoad
        # - send "drop load" to controller
        self.controller_adapter_delegate.speed_reached()
        time.sleep(self.__time_delay)
        self.line_detection.stop_tracking.assert_called_with()
        self.controller_adapter.set_status.assert_called_with(Status.EmptyingLoad)
        self.container_recognition.drop_load.assert_called_with()

        # Step 3
        # Controller says "load dropped". Operator should:
        # - set status to End.
        self.controller_adapter_delegate.speed_reached()
        time.sleep(self.__time_delay)
        self.controller_adapter.set_status.assert_called_with(Status.End)

    def __go_to_ready(self):
        self.operatorthread = threading.Thread(target=self.operator.run, name="Operator_MainThread")
        self.operatorthread.start()
        time.sleep(self.__time_delay)
        self.controller_adapter_delegate.ready()
        time.sleep(self.__time_delay)
        self.controller_adapter.set_status.assert_called_with(Status.Ready)
