import unittest
from unittest.mock import MagicMock

from multiprocessing import Queue

from ah3k_management.controller_adapter.ControllerAdapterImpl import ControllerAdapterImpl
from ah3k_management.operator.Resources import Resources
from ah3k_management.operator.actions.SetDrivingStatusAction import SetDrivingStatusAction
from ah3k_management.operator.actions.SpeedAction import SpeedAction
from ah3k_management.operator.actions.StartContainerRecognitionAction import StartContainerRecognitionAction
from ah3k_management.operator.actions.StartLineDetectionAction import StartLineDetectionAction
from ah3k_management.operator.actions.StoreContainerColorAction import StoreContainerColorAction
from ah3k_management.operator.inputs.ContainerDroppedInput import ContainerDroppedInput
from ah3k_management.operator.inputs.StartInput import StartInput
from ah3k_management.shared.ContainerColor import ContainerColor
from ah3k_management.shared.DrivingStatus import DrivingStatus


class TestOperatorActions(unittest.TestCase):
    def setUp(self):
        queue = Queue()

        # Controller Adapter
        self.controller_adapter = MagicMock()
        self.controller_adapter.get_interface_version = MagicMock(return_value=ControllerAdapterImpl.version)
        self.controller_adapter.version = ControllerAdapterImpl.version


        # Line Detection
        self.line_detection = MagicMock()

        # Container Recognition
        self.container_recognition = MagicMock()

        # noinspection PyTypeChecker
        self.resources = Resources(line_detection=self.line_detection, container_recognition=self.container_recognition,
                                   controller_adapter=self.controller_adapter, input_queue=queue)


    def test_StoreContainerColorAction_withStartInput_greenContainer(self):
        # Arrange
        color = ContainerColor.Green
        input = StartInput(color)
        action = StoreContainerColorAction(self.resources)

        # Act
        action.run(input)

        # Assert
        self.assertEqual(self.resources.container_color, color)

    def test_StoreContainerColorAction_withStartInput_blueContainer(self):
        # Arrange
        color = ContainerColor.Blue
        input = StartInput(color)
        action = StoreContainerColorAction(self.resources)

        # Act
        action.run(input)

        # Assert
        self.assertEqual(self.resources.container_color, color)

    def test_ContainerRecognitionAction_withStartInput_greenContainer(self):
        # Arrange
        color = ContainerColor.Green
        input = StartInput(ContainerColor.Blue)
        action = StartContainerRecognitionAction(self.resources)
        self.resources.container_color = color

        # Act
        action.run(input)

        # Assert
        self.container_recognition.start_tracking.assert_called_with(color)

    def test_ContainerRecognition_withContainerDroppedInput_blueContainer(self):
        # Arrange
        color = ContainerColor.Blue
        input = ContainerDroppedInput()
        action = StartContainerRecognitionAction(self.resources)
        self.resources.container_color = color

        # Act
        action.run(input)

        # Assert
        self.container_recognition.start_tracking.assert_called_with(color)

    def test_StartLineDetectionAction_withStartInput(self):
        # Arrange
        input = StartInput(ContainerColor.Blue)
        action = StartLineDetectionAction(self.resources)

        # Act
        action.run(input)

        # Assert
        self.line_detection.start_tracking.assert_called_with()


    def test_StartLineDetectionAction_withContainerDroppedInput(self):
        # Arrange
        input = ContainerDroppedInput()
        action = StartLineDetectionAction(self.resources)

        # Act
        action.run(input)

        # Assert
        self.line_detection.start_tracking.assert_called_with()

    def test_SetDrivingStatusAction_accelerating(self):
        # Arrange
        d_status = DrivingStatus.Accelerating
        input = StartInput(ContainerColor.Blue)
        action = SetDrivingStatusAction(self.resources, d_status)

        # Act
        action.run(input)

        # Assert
        self.assertEqual(d_status, self.resources.driving_status)

    def test_SetDrivingStatusAction_stopping(self):
        # Arrange
        d_status = DrivingStatus.Stopping
        input = StartInput(ContainerColor.Blue)
        action = SetDrivingStatusAction(self.resources, d_status)

        # Act
        action.run(input)

        # Assert
        self.assertEqual(d_status, self.resources.driving_status)

    def test_SpeedAction(self):
        # Arrange
        speed = 35
        input = StartInput(ContainerColor.Blue)
        action = SpeedAction(self.resources, speed)

        # Act
        action.run(input)

        # Assert
        self.assertEqual(speed, self.resources.current_speed)
        self.controller_adapter.speed.assert_called_with(speed)

    def test_SpeedAction_2(self):
        # Arrange
        speed = -45
        input = StartInput(ContainerColor.Blue)
        action = SpeedAction(self.resources, speed)

        # Act
        action.run(input)

        # Assert
        self.assertEqual(speed, self.resources.current_speed)
        self.controller_adapter.speed.assert_called_with(speed)
