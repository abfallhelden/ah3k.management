
import unittest

from ah3k_management.operator.Rule import Rule
from ah3k_management.operator.inputs.EndLineInput import EndLineInput
from ah3k_management.operator.inputs.SpeedReachedInput import SpeedReachedInput
from ah3k_management.shared.DrivingStatus import DrivingStatus
from ah3k_management.shared.Status import Status


class TestInputTypeIds(unittest.TestCase):

    def test_id(self):
        value = SpeedReachedInput
        self.assertEqual(13, value.input_id())

    def test_hashOfRule(self):
        rule1 = Rule(SpeedReachedInput, Status.Driving, DrivingStatus.Moving, [None])
        rule2 = Rule(EndLineInput, Status.EmptyingContainer, DrivingStatus.Stopping, [None])

        rules = {rule1.hash(): rule1, rule2.hash(): rule2}

        result1 = rules[Rule.hash_of_input_and_status(SpeedReachedInput, Status.Driving, DrivingStatus.Moving)]
        result2 = rules[Rule.hash_of_input_and_status(EndLineInput, Status.EmptyingContainer, DrivingStatus.Stopping)]

        self.assertEqual(rule1, result1)
        self.assertEqual(rule2, result2)

    def test_withObject(self):
        rule2 = Rule(EndLineInput, Status.EmptyingContainer, DrivingStatus.Standing, [None])
        input3 = EndLineInput()


        rules = {rule2.hash(): rule2}

        result3 = rules[Rule.hash_of_input_and_status(input3, Status.EmptyingContainer, DrivingStatus.Standing)]

        self.assertEqual(rule2, result3)

