import threading

from ah3k_management.line_detection.interface.LineInfo import LineInfo
from ah3k_management.operator.Operator import Operator
from ah3k_management.operator.inputs.BatteryLowInput import BatteryLowInput
from ah3k_management.operator.inputs.CrossroadDetectedInput import CrossroadDetectedInput
from ah3k_management.operator.inputs.StartInput import StartInput
from ah3k_management.shared.ContainerColor import ContainerColor
from ah3k_management.shared.Status import Status


# noinspection PyMethodMayBeStatic,PyUnusedLocal
class UserConsole:
    def __init__(self, operator: Operator):
        self.operator = operator
        self.commands = self.build_commands()
        self.operatorthread = threading.Thread(target=self.operator.run, name="Operator_MainThread")

    def help(self):
        print("Du kannst folgende Befehle ausführen:\n\n"
              "s | start [blue | green]\t\tStartet die Fahrt\n"
              "r | radius [mm]\t\tStellt den Raduis der Kurve ein\n"
              "speed [mm/s]\t\tStellt die aktuelle Geschwindigkeit ein\n"
              "line [mm_front]\t\tSendet eine LineInfo an den Operator.\n"
              "status\t\t\tGibt den Status aus.\n"
              "drivingstatus\t\tGibt den DrivingStatus aus.\n"
              "threads\t\t\tGibt die Anzahl aktiver Threads aus.\n"
              "stop\t\t\tStoppt die fahrt und setzt den status auf Ende.\n"
              "writeconfig\t\tSpeichert die aktuelle Konfiguration\n"
              "crossroad\t\tSimuliert die Erkennung der Kreuzung\n"
              "send [message]\t\tSendet eine Nachricht an den controller.\n"
              "e | exit\t\t\tführt 'stop' aus und beendet die Anwendung\n")

    def run(self):
        print("**********************************")
        print("* Willkommen zum Abfallheld 3000 *")
        print("**********************************\n")

        self.help()
        self._init([])

        command = ""
        while command != "exit" and command != "e":
            command = input("> ")
            if command:
                self.execute(command)

    def execute(self, command: str):

        words = command.split(" ")
        command_name = words[0]
        args = words[1:]

        executable = self.commands.get(command_name)
        if executable:
            executable(args)
        else:
            print("{} is not a known command. Type 'help' for more information.".format(command_name))

    def build_commands(self) -> {}:
        commands = {"status": self._status,
                    "drivingstatus": self._driving_status,
                    "help": self._help,
                    "exit": self._exit,
                    "e": self._exit,
                    "stop": self._stop,
                    "radius": self._radius,
                    "r": self._radius,
                    "speed": self._speed,
                    "line": self._line,
                    "writeconfig": self._writeconfig,
                    "threads": self._threads,
                    "start": self._start,
                    "s": self._start,
                    "send": self._send,
                    "crossroad": self._crossroad}

        return commands

    def _help(self, args):
        self.help()

    def _init(self, args):
        self.operatorthread.start()

    def _status(self, args):
        print(self.operator.resources.status)

    def _driving_status(self, args):
        print(self.operator.resources.driving_status)

    def _stop(self, args):
        print("sending battery low notification")
        self.operator.input_handler.add_input(BatteryLowInput())

    def _exit(self, args):
        self._stop(args)
        print("bye")

    def _threads(self, args):
        print(threading.active_count())

    def _writeconfig(self, args):
        self.operator.resources.configuration.save()

    def _radius(self, args: []):
        radius = int(args[0])
        self.operator.resources.controller_adapter.steer(radius)

    def _speed(self, args: []):
        speed = int(args[0])
        self.operator.resources.controller_adapter.speed(speed)

    def _line(self, args: []):
        line_info = LineInfo(int(args[0]))
        self.operator.input_collector.line_info(line_info)

    def _send(self, args: []):
        message = " ".join(args)
        self.operator.resources.controller_adapter.send_line(message)

    def _start(self, args):
        if self.operator.resources.status != Status.Ready:
            print("Status is not 'Ready'. Can not start.")

        if len(args) > 0:
            color_text = args[0]
        else:
            color_text = "green"

        if color_text == "blue":
            color = ContainerColor.Blue
        elif color_text == "green":
            color = ContainerColor.Green
        else:
            print("{} is not a known container color. Use 'start blue' or 'start green'".format(color_text))
            return

        print("Starting with {}".format(color))
        self.operator.input_handler.add_input(StartInput(color))

    def _crossroad(self, args):
        self.operator.input_handler.add_input(CrossroadDetectedInput())
