#!/usr/bin/env bash
echo Transferring...
rsync -avz --exclude '*.mov' ~/workspace/ah3k/management/src/* -e ssh pi@$1:/home/pi/ah3k
echo
echo
echo Installing...
ssh pi@$1 "cd /home/pi/ah3k; sudo python3 setup.py install"